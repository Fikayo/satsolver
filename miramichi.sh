#!/bin/bash

java \
    -Djava.util.logging.config.file=logging.properties \
    -classpath ./bin/:./lib/jcommander-1.48.jar:./lib/JOCL-0.1.9.jar \
    ca.uwaterloo.miramichi.Main \
    $*

