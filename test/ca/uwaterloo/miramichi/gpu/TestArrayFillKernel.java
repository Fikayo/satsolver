package ca.uwaterloo.miramichi.gpu;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import org.jocl.Sizeof;
import org.jocl.cl_event;
import org.junit.Test;

import ca.uwaterloo.miramichi.gpu.Kernel;
import ca.uwaterloo.miramichi.gpu.Mem;
import ca.uwaterloo.miramichi.gpu.MemCapacity;
import ca.uwaterloo.miramichi.gpu.MemConfig;
import ca.uwaterloo.miramichi.gpu.MemInt;
import ca.uwaterloo.miramichi.gpu.OpenCLComputation;
import ca.uwaterloo.miramichi.gpu.OpenCLComputer;
import ca.uwaterloo.miramichi.gpu.OpenCLUtil;

public class TestArrayFillKernel {

  @Test
  public void test() {

    final OpenCLComputation d = new ZeroComputation();
    final OpenCLComputer c = new OpenCLComputer(true, true, 35, d);

    c.initialize();
    for (int i = 0; i < 100; i++) {
      c.compute();
    }
    c.release();

  }

}


class ZeroComputation implements OpenCLComputation {

  private final static boolean GPU_PROFILING = true;
  private final static int VALUE = 7; // value to put in the array

  private final static String kernel_file_zero_int = "src/ca/uwaterloo/miramichi/gpu/zero_int.cl";
  private final static String kernel_file_array_fill_int_unroll =
      "src/ca/uwaterloo/miramichi/gpu/array_fill_int_unroll.cl";


  /** Zero kernels are used to zero out arrays on the GPU. The order of these is hard-coded. */
  private Kernel[] kernels = new Kernel[] {new Kernel(kernel_file_zero_int, "zero_int") {

    @Override
    public cl_event prepareLaunchInner() {
      setArg("a", Sizeof.cl_mem, A.d_pointer());
      setArg("length", Sizeof.cl_int, OpenCLUtil.pointerToInt(A.capacity.deviceCapacity()));

      // launch zero_int kernel
      final cl_event kernel_execution_event = GPU_PROFILING ? new cl_event() : null;
      final int work_items = A.capacity.deviceCapacity();
      total_work_items += work_items;

      work_dim = 1;
      global_work_offset = null;
      global_work_size = new long[] {work_items};
      local_work_size = null;
      num_events_in_wait_list = 0;
      event_wait_list = null;
      event = kernel_execution_event;

      return kernel_execution_event;
    }

  }, new Kernel(kernel_file_array_fill_int_unroll, "array_fill_int_unroll") {

    private final int unroll = 8;
    private int step = 1; // will be reset below

    @Override
    public cl_event prepareLaunchInner() {
      setArg("a", Sizeof.cl_mem, A.d_pointer());
      setArg("length", Sizeof.cl_int, OpenCLUtil.pointerToInt(A.capacity.deviceCapacity()));

      // prepare to launch kernel
      final cl_event kernel_execution_event = GPU_PROFILING ? new cl_event() : null;
      final int work_items = A.capacity.deviceCapacity() / unroll;
      total_work_items += work_items;

      assert (A.capacity.deviceCapacity() / step) * step == A.capacity.deviceCapacity() : "work is not properly sized for this array";

      work_dim = 1;
      global_work_offset = null;
      global_work_size = new long[] {work_items};
      local_work_size = new long[] {maxWorkgroupSize};
      num_events_in_wait_list = 0;
      event_wait_list = null;
      event = kernel_execution_event;

      return kernel_execution_event;
    }

    @Override
    protected List<String> preprocessorDirectives() {
      final List<String> p = super.preprocessorDirectives();
      p.add("#define UNROLL " + unroll);
      step = (int) maxWorkgroupSize;// A.capacity.deviceCapacity() / unroll;
      p.add("#define STEP " + step);
      p.add("#define VALUE " + VALUE);
      for (final String s : p) {
        System.out.println(s);
      }
      return p;
    }
  }};

  private final MemInt A = new MemInt("A", MemConfig.UploadDownload, new MemCapacity(
      () -> 1024 * 1024));

  private final Mem[] memories = new Mem[] {A};

  @Override
  public Mem[] memories() {
    return memories;
  }

  @Override
  public Kernel[] kernels() {
    return kernels;
  }

  @Override
  public void uploadPreprocessing() {
    Arrays.fill(A.h, 1);
  }

  @Override
  public void uploadPostprocessing() {
    return;
  }

  @Override
  public void downloadPreprocessing() {
    return;
  }

  @Override
  public void downloadPostprocessing() {
    assert A.h_length() == A.capacity.hostCapacity();
    for (int i = 0; i < A.h_length(); i++) {
      assert A.h[i] == VALUE : "index was not filled. A.h[0] == " + A.h[0] + ".  A.h[" + i
          + "] == " + A.h[i];
    }
  }

  @Override
  public Logger logger() {
    return logger;
  }

  private final Logger logger = Logger.getLogger("TestArrayFillKernel");

}
