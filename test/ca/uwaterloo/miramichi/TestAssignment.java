package ca.uwaterloo.miramichi;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestAssignment {

  @Test
  public void test1() {
    final Assignment a = new Assignment(100);
    for (int i = 0; i < 100; i++) {
      a.set(i, i % 3);
    }
    for (int i = 0; i < 100; i++) {
      assertEquals(i % 3, a.get(i));
    }
  }

  @Test
  public void test2() {
    final Assignment a = new Assignment(100);
    final int[] b = new int[100];
    for (int i = 0; i < 100; i++) {
      final int v = Math.random() > 0.5 ? 0 : 1;
      a.set(i, v);
      b[i] = v;
    }
    for (int i = 0; i < 100; i++) {
      assertEquals(b[i], a.get(i));
    }
  }

  @Test
  public void testToString1() {
    final Assignment a = new Assignment(1);
    assertEquals("[T]", a.toString());
  }

  @Test
  public void testToString2() {
    final Assignment a = new Assignment(1);
    a.set(0, 1);
    assertEquals("[F]", a.toString());
  }

  @Test
  public void testToString3() {
    final Assignment a = new Assignment(1);
    a.set(0, 2);
    assertEquals("[?]", a.toString());
  }

  @Test
  public void testToString4() {
    final Assignment a = new Assignment(2);
    assertEquals("[T, T]", a.toString());
  }

  @Test
  public void testSubAssignment() {
    final int subCount = 10;
    final int subLength = 10;
    final Assignment a = new Assignment(subCount * subLength);
    final int[] b = new int[a.size()];
    for (int i = 0; i < a.size(); i++) {
      final int v = i % 3;
      a.set(i, v);
      b[i] = v;
    }
    for (int i = 0; i < subCount; i++) {
      final int baseIndex = i * subLength;
      final Assignment s = a.subAssignment(baseIndex, subLength);
      for (int j = 0; j < s.size(); j++) {
        final int expected = b[baseIndex + j];
        assertEquals(expected, s.get(j));
        s.set(j, 0);
        assertEquals(0, s.get(j));
      }
    }
  }

  @Test
  public void testOffsetRemainder() {
    for (int i = 0; i < 100; i++) {
      final int offset = Assignment.offset(i);
      final int remainder = Assignment.remainder(i);
      final int index = Assignment.index(offset, remainder);
      System.out.println(Util.i2s(i, offset, offset * 4, remainder, (remainder >>> 1), index));
      assertEquals(i, index);
    }
  }
}
