package ca.uwaterloo.miramichi;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Random;

import org.junit.Test;

public class TestPackedArray {

  @Test
  public void testConstructor1() {
    final PackedArray a = new PackedArray(1, 1);
    assertEquals(1, a.bitwidth);
    assertEquals(32, a.valuesPerCell);
    assertEquals(1, a.valueMask);
    assertEquals(1, a.size());
  }

  @Test
  public void testConstructor2() {
    final PackedArray a = new PackedArray(2, 1);
    assertEquals(2, a.bitwidth);
    assertEquals(16, a.valuesPerCell);
    assertEquals(3, a.valueMask);
    assertEquals(1, a.size());
  }

  @Test
  public void testConstructor7() {
    final PackedArray a = new PackedArray(7, 1);
    assertEquals(8, a.bitwidth);
    assertEquals(4, a.valuesPerCell);
    assertEquals(255, a.valueMask);
    assertEquals(1, a.size());
  }

  @Test
  public void testConstructor8() {
    final PackedArray a = new PackedArray(8, 1);
    assertEquals(8, a.bitwidth);
    assertEquals(4, a.valuesPerCell);
    assertEquals(255, a.valueMask);
    assertEquals(1, a.size());
  }

  @Test
  public void testConstructor9() {
    final PackedArray a = new PackedArray(9, 1);
    assertEquals(16, a.bitwidth);
    assertEquals(2, a.valuesPerCell);
    assertEquals(65536 - 1, a.valueMask);
    assertEquals(1, a.size());
  }

  @Test
  public void testConstructor17() {
    final PackedArray a = new PackedArray(17, 1);
    assertEquals(32, a.bitwidth);
    assertEquals(1, a.valuesPerCell);
    assertEquals(0xFFFFFFFF, a.valueMask);
    // assertEquals(Integer.MAX_VALUE, a.mask);
    assertEquals(1, a.size());
  }

  @Test
  public void testConstructor31() {
    final PackedArray a = new PackedArray(31, 1);
    assertEquals(32, a.bitwidth);
    assertEquals(1, a.valuesPerCell);
    assertEquals(0xFFFFFFFF, a.valueMask);
    assertEquals(1, a.size());
  }

  @Test
  public void testConstructor32() {
    final PackedArray a = new PackedArray(32, 1);
    assertEquals(32, a.bitwidth);
    assertEquals(1, a.valuesPerCell);
    assertEquals(0xFFFFFFFF, a.valueMask);
    assertEquals(1, a.size());
  }

  @Test
  public void testSetGet0() {
    final PackedArray a = new PackedArray(8, 1);
    for (int i = 0; i < 256; i++) {
      a.set(0, i);
      assertEquals(i, a.get(0));
    }
  }

  @Test
  public void testSetGet1() {
    final PackedArray a = new PackedArray(8, 3);
    // everything should be zero to start
    assertEquals(0, a.get(0));
    assertEquals(0, a.get(1));
    assertEquals(0, a.get(2));
    // try setting 1 in the second position
    a.set(1, 1);
    assertEquals(1, a.get(1));
    assertEquals(0, a.get(0));
    assertEquals(0, a.get(2));

    // try setting some values at the second position (i.e., index 1)
    for (int i = 0; i < 255; i++) {
      a.set(1, i);
      assertEquals("i==" + i, 0, a.get(0));
      assertEquals("i==" + i, i, a.get(1));
      assertEquals("i==" + i, 0, a.get(2));
    }
  }

  @Test
  public void testCell() {
    final int initialSize = 5;
    final PackedArray a = new PackedArray(8, initialSize);
    assertEquals(0, a.cell(0));
    assertEquals(0, a.cell(1));
    assertEquals(0, a.cell(2));
    assertEquals(0, a.cell(3));
    assertEquals(1, a.cell(4));
    assertEquals(1, a.cell(5));
    assertEquals(1, a.cell(6));
    assertEquals(1, a.cell(7));
    assertEquals(2, a.cell(8));
    assertEquals(2, a.cell(9));
    assertEquals(2, a.cell(10));
    assertEquals(2, a.cell(11));
  }

  @Test
  public void testSubcell() {
    final int initialSize = 100;
    for (int bitwidth = 2; bitwidth < 32; bitwidth++) {
      testSubcell(initialSize, bitwidth);
    }
  }

  private void testSubcell(final int initialSize, final int bitwidth) {
    final PackedArray a = new PackedArray(bitwidth, initialSize);
    int index = 0;
    for (int cell = 0; cell < a.valuesPerCell; cell++) {
      for (int subcell = 0; subcell < 32; subcell += a.bitwidth) {
        // System.out.println(Util.i2s(index, cell, subcell) + " " +
        // Integer.toBinaryString(subcell));
        assertEquals("cell", cell, a.cell(index));
        assertEquals("subcell", subcell, a.subcell(index));
        index++;
      }
    }
  }

  @Test
  public void testSetGet2() {
    final int initialSize = 5;
    final PackedArray a = new PackedArray(8, initialSize);

    final int m = 5;
    for (int i = 0; i < initialSize; i++) {
      a.set(i, i % m);
    }
    for (int i = 0; i < initialSize; i++) {
      assertEquals("i==" + i, i % m, a.get(i));
    }
  }

  @Test
  public void testSetGet3() {
    final int initialSize = 10;
    final PackedArray a = new PackedArray(2, initialSize);
    final int[] b = new int[initialSize];
    for (int i = 0; i < initialSize; i++) {
      final int v = Math.random() > 0.5 ? 0 : 1;
      a.set(i, v);
      b[i] = v;
    }
    for (int i = 0; i < initialSize; i++) {
      assertEquals("i: " + i, b[i], a.get(i));
    }
  }

  @Test
  public void testRandom() {
    final Random r = new Random();
    for (int i = 0; i < 1000; i++) {
      final int bitwidth = r.nextInt(30) + 1;
      final int size = r.nextInt(1000);
      testRandom(r, size, bitwidth);
    }
  }

  private void testRandom(final Random r, final int initialSize, final int bitwidth) {
    final PackedArray a = new PackedArray(bitwidth, initialSize);
    final int[] b = new int[initialSize];
    for (int i = 0; i < initialSize; i++) {
      final int v = r.nextInt(a.maxValue());
      a.set(i, v);
      b[i] = v;
    }
    for (int i = 0; i < initialSize; i++) {
      assertEquals("bitwidth, size, i: " + Util.i2s(bitwidth, initialSize, i), b[i], a.get(i));
    }
  }

  @Test
  public void testLeftovers() {
    final PackedArray a = new PackedArray(8, 7);
    assertFalse(a.hasLeftovers());
    final PackedArray a2 = a.getLeftovers();
    assertEquals(0, a2.size());


    a.set(0, 1);
    assertTrue(a.hasLeftovers());
    final PackedArray b = a.getLeftovers();
    assertEquals(1, b.size());
    assertEquals(1, b.get(0));

    a.set(3, 4);
    assertFalse(a.hasLeftovers());

    a.set(6, 7);
    assertTrue(a.hasLeftovers());
    final PackedArray c = a.getLeftovers();
    assertEquals(3, c.size());
    assertEquals(0, c.get(0));
    assertEquals(0, c.get(1));
    assertEquals(7, c.get(2));

  }
}
