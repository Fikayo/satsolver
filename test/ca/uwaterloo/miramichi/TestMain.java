package ca.uwaterloo.miramichi;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.logging.Logger;

import org.junit.Test;

public class TestMain {

  private static final Logger logger = Logger.getLogger(TestMain.class.getName());

  private final ArrayList<String> args = new ArrayList<>();

  public TestMain() {
    // check the environment to find out what engine to use
    // environment can be set in Eclipse run configuration

    // file placeholder
    args.add("FileNameGoesHere");

    // engine
    final String engine = System.getenv("TestMainEngine");
    if (null != engine) {
      args.add("-engine");
      args.add(engine);
      args.add("-gpuprof");
    }

    // how many explorers to create initially?
    final String initialCrewSizeString = System.getenv("TestMainInitialCrewSize");
    if (null != initialCrewSizeString) {
      args.add("-initialCrewSize");
      args.add(initialCrewSizeString);
    }

    // what seeding strategy to use?
    final String seedingStrategyString = System.getenv("TestMainSeedingStrategy");
    if (null != seedingStrategyString) {
      args.add("-seeding");
      args.add(seedingStrategyString);
    }

    // explorer
    if ("Comparison".equals(engine)) {
      args.add("-explorer");
      args.add("Comparison");
    }
  }

  private boolean runSatSolver(String path) throws InterruptedException {
    // set path in the first position of args
    args.set(0, path);
    // run
    Main.main(args.toArray(new String[args.size()]));
    return Main.result.toBoolean();
  }

  @Test
  public void test1sat() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat/1_simple_sat.cnf"));
  }

  @Test
  public void test1unsat() throws InterruptedException {
    assertFalse(runSatSolver("cnf/unsat/1_simple_unsat.cnf"));
  }

  @Test
  public void test2unsat() throws InterruptedException {
    assertFalse(runSatSolver("cnf/unsat/3_unsat.cnf"));
  }

  @Test
  public void test3unsat() throws InterruptedException {
    assertFalse(runSatSolver("cnf/unsat/4_unsat.cnf"));
  }

  public void testAim50SAT1() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/aim-50-1_6-yes1-1.cnf"));
  }

  @Test
  public void testAim50SAT2() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/aim-50-1_6-yes1-2.cnf"));
  }

  @Test
  public void testAim50SAT3() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/aim-50-1_6-yes1-3.cnf"));
  }

  @Test
  public void testAim50SAT4() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/aim-50-1_6-yes1-4.cnf"));
  }

  @Test
  public void testAim50SAT5() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/aim-50-2_0-yes1-1.cnf"));
  }

  @Test
  public void testAim50SAT6() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/aim-50-2_0-yes1-2.cnf"));
  }

  @Test
  public void testAim50SAT7() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/aim-50-2_0-yes1-3.cnf"));
  }

  @Test
  public void testAim50SAT8() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/aim-50-2_0-yes1-4.cnf"));
  }

  @Test
  public void testAim50SAT9() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/aim-50-3_4-yes1-1.cnf"));
  }

  @Test
  public void testAim50SAT10() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/aim-50-3_4-yes1-2.cnf"));
  }

  @Test
  public void testAim50SAT11() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/aim-50-3_4-yes1-3.cnf"));
  }

  @Test
  public void testAim50SAT12() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/aim-50-3_4-yes1-4.cnf"));
  }

  @Test
  public void testAim50SAT13() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/aim-50-6_0-yes1-1.cnf"));
  }

  @Test
  public void testAim50SAT14() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/aim-50-6_0-yes1-2.cnf"));
  }

  @Test
  public void testAim50SAT15() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/aim-50-6_0-yes1-3.cnf"));
  }

  @Test
  public void testAim50SAT16() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/aim-50-6_0-yes1-4.cnf"));
  }

  @Test
  public void testAim50UNSAT1() throws InterruptedException {
    assertFalse(runSatSolver("cnf/sat4j_import/aim-50-1_6-no-1.cnf"));
  }

  @Test
  public void testAim50UNSAT2() throws InterruptedException {
    assertFalse(runSatSolver("cnf/sat4j_import/aim-50-1_6-no-2.cnf"));
  }

  @Test
  public void testAim50UNSAT3() throws InterruptedException {
    assertFalse(runSatSolver("cnf/sat4j_import/aim-50-1_6-no-3.cnf"));
  }

  @Test
  public void testAim50UNSAT4() throws InterruptedException {
    assertFalse(runSatSolver("cnf/sat4j_import/aim-50-1_6-no-4.cnf"));
  }

  @Test
  public void testAim50UNSAT5() throws InterruptedException {
    assertFalse(runSatSolver("cnf/sat4j_import/aim-50-2_0-no-1.cnf"));
  }

  @Test
  public void testAim50UNSAT6() throws InterruptedException {
    assertFalse(runSatSolver("cnf/sat4j_import/aim-50-2_0-no-2.cnf"));
  }

  @Test
  public void testAim50UNSAT7() throws InterruptedException {
    assertFalse(runSatSolver("cnf/sat4j_import/aim-50-2_0-no-3.cnf"));
  }

  @Test
  public void testAim50UNSAT8() throws InterruptedException {
    assertFalse(runSatSolver("cnf/sat4j_import/aim-50-2_0-no-4.cnf"));
  }

  @Test
  public void testIi1() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/ii8a1.cnf"));
  }

  @Test
  public void testIi2() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/ii8a2.cnf"));
  }

  @Test
  public void testIi3() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/ii8a3.cnf"));
  }

  @Test
  public void testIi4() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/ii8a4.cnf"));
  }

  @Test
  public void testIi5() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/ii8b1.cnf"));
  }

  @Test
  public void testIi6() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/ii8b2.cnf"));
  }

  @Test
  public void testIi7() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/ii8b3.cnf"));
  }

  @Test
  public void testIi8() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/ii8b4.cnf"));
  }

  @Test
  public void testIi9() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/ii8c1.cnf"));
  }

  @Test
  public void testIi10() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/ii8c2.cnf"));
  }

  @Test
  public void testIi11() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/ii8d1.cnf"));
  }

  @Test
  public void testIi12() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/ii8d2.cnf"));
  }

  @Test
  public void testIi13() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/ii8e1.cnf"));
  }

  @Test
  public void testIi14() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/ii8e2.cnf"));
  }

  @Test
  public void testIi15() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/ii16a1.cnf"));
  }

  @Test
  public void testIi16() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/ii16a2.cnf"));
  }

  @Test
  public void testIi17() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/ii16b1.cnf"));
  }

  @Test
  public void testIi18() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/ii16b2.cnf"));
  }

  // public void testIi19()
  // throws InterruptedException {
  // }

  @Test
  public void testIi20() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/ii16c2.cnf"));
  }

  @Test
  public void testIi21() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/ii16d1.cnf"));
  }

  @Test
  public void testIi22() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/ii16d2.cnf"));
  }

  @Test
  public void testIi23() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/ii16e1.cnf"));
  }

  @Test
  public void testJNH1() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/jnh1.cnf"));
  }

  @Test
  public void testJNH2() throws InterruptedException {
    assertFalse(runSatSolver("cnf/sat4j_import/jnh2.cnf"));
  }

  @Test
  public void testJNH3() throws InterruptedException {
    assertFalse(runSatSolver("cnf/sat4j_import/jnh3.cnf"));
  }

  @Test
  public void testJNH4() throws InterruptedException {
    assertFalse(runSatSolver("cnf/sat4j_import/jnh4.cnf"));
  }

  @Test
  public void testJNH5() throws InterruptedException {
    assertFalse(runSatSolver("cnf/sat4j_import/jnh5.cnf"));
  }

  @Test
  public void testJNH6() throws InterruptedException {
    assertFalse(runSatSolver("cnf/sat4j_import/jnh6.cnf"));
  }

  @Test
  public void testJNH7() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/jnh7.cnf"));
  }

  @Test
  public void testJNH8() throws InterruptedException {
    assertFalse(runSatSolver("cnf/sat4j_import/jnh8.cnf"));
  }

  @Test
  public void testJNH9() throws InterruptedException {
    assertFalse(runSatSolver("cnf/sat4j_import/jnh9.cnf"));
  }

  @Test
  public void testJNH10() throws InterruptedException {
    assertFalse(runSatSolver("cnf/sat4j_import/jnh10.cnf"));
  }

  @Test
  public void testJNH11() throws InterruptedException {
    assertFalse(runSatSolver("cnf/sat4j_import/jnh11.cnf"));
  }

  @Test
  public void testJNH12() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/jnh12.cnf"));
  }

  @Test
  public void testJNH13() throws InterruptedException {
    assertFalse(runSatSolver("cnf/sat4j_import/jnh13.cnf"));
  }

  @Test
  public void testJNH14() throws InterruptedException {
    assertFalse(runSatSolver("cnf/sat4j_import/jnh14.cnf"));
  }

  @Test
  public void testJNH15() throws InterruptedException {
    assertFalse(runSatSolver("cnf/sat4j_import/jnh15.cnf"));
  }

  @Test
  public void testJNH16() throws InterruptedException {
    assertFalse(runSatSolver("cnf/sat4j_import/jnh16.cnf"));
  }

  @Test
  public void testJNH17() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/jnh17.cnf"));
  }

  @Test
  public void testJNH18() throws InterruptedException {
    assertFalse(runSatSolver("cnf/sat4j_import/jnh18.cnf"));
  }

  @Test
  public void testJNH19() throws InterruptedException {
    assertFalse(runSatSolver("cnf/sat4j_import/jnh19.cnf"));
  }

  @Test
  public void testJNH20() throws InterruptedException {
    assertFalse(runSatSolver("cnf/sat4j_import/jnh20.cnf"));
  }

  @Test
  public void testJNH21() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/jnh201.cnf"));
  }

  @Test
  public void testJNH22() throws InterruptedException {
    assertFalse(runSatSolver("cnf/sat4j_import/jnh202.cnf"));
  }

  @Test
  public void testJNH23() throws InterruptedException {
    assertFalse(runSatSolver("cnf/sat4j_import/jnh203.cnf"));
  }

  @Test
  public void testJNH24() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/jnh204.cnf"));
  }

  @Test
  public void testJNH25() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/jnh205.cnf"));
  }

  @Test
  public void testJNH26() throws InterruptedException {
    assertFalse(runSatSolver("cnf/sat4j_import/jnh206.cnf"));
  }

  @Test
  public void testJNH27() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/jnh207.cnf"));
  }

  @Test
  public void testJNH28() throws InterruptedException {
    assertFalse(runSatSolver("cnf/sat4j_import/jnh208.cnf"));
  }

  @Test
  public void testJNH29() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/jnh209.cnf"));
  }

  @Test
  public void testJNH30() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/jnh210.cnf"));
  }

  @Test
  public void testJNH31() throws InterruptedException {
    assertFalse(runSatSolver("cnf/sat4j_import/jnh211.cnf"));
  }

  @Test
  public void testJNH32() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/jnh212.cnf"));
  }

  @Test
  public void testJNH33() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/jnh213.cnf"));
  }

  @Test
  public void testJNH34() throws InterruptedException {
    assertFalse(runSatSolver("cnf/sat4j_import/jnh214.cnf"));
  }

  @Test
  public void testJNH35() throws InterruptedException {
    assertFalse(runSatSolver("cnf/sat4j_import/jnh215.cnf"));
  }

  @Test
  public void testJNH36() throws InterruptedException {
    assertFalse(runSatSolver("cnf/sat4j_import/jnh216.cnf"));
  }

  @Test
  public void testJNH37() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/jnh217.cnf"));
  }

  @Test
  public void testJNH38() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/jnh218.cnf"));
  }

  @Test
  public void testJNH39() throws InterruptedException {
    assertFalse(runSatSolver("cnf/sat4j_import/jnh219.cnf"));
  }

  @Test
  public void testJNH40() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/jnh220.cnf"));
  }

  @Test
  public void testJNH41() throws InterruptedException {
    assertTrue(runSatSolver("cnf/sat4j_import/jnh301.cnf"));
  }

  @Test
  public void testJNH42() throws InterruptedException {
    assertFalse(runSatSolver("cnf/sat4j_import/jnh302.cnf"));
  }

  @Test
  public void testJNH43() throws InterruptedException {
    assertFalse(runSatSolver("cnf/sat4j_import/jnh303.cnf"));
  }

  @Test
  public void testJNH44() throws InterruptedException {
    assertFalse(runSatSolver("cnf/sat4j_import/jnh304.cnf"));
  }

  @Test
  public void testJNH45() throws InterruptedException {
    assertFalse(runSatSolver("cnf/sat4j_import/jnh305.cnf"));
  }

  @Test
  public void testJNH46() throws InterruptedException {
    assertFalse(runSatSolver("cnf/sat4j_import/jnh306.cnf"));
  }

  @Test
  public void testJNH47() throws InterruptedException {
    assertFalse(runSatSolver("cnf/sat4j_import/jnh307.cnf"));
  }

  @Test
  public void testJNH48() throws InterruptedException {
    assertFalse(runSatSolver("cnf/sat4j_import/jnh308.cnf"));
  }

  @Test
  public void testJNH49() throws InterruptedException {
    assertFalse(runSatSolver("cnf/sat4j_import/jnh309.cnf"));
  }

  @Test
  public void testJNH50() throws InterruptedException {
    assertFalse(runSatSolver("cnf/sat4j_import/jnh310.cnf"));
  }

  @Test
  public void testHole6() throws InterruptedException {
    assertFalse(runSatSolver("cnf/sat4j_import/hole6.cnf"));
  }

  @Test
  public void testHole7() throws InterruptedException {
    assertFalse(runSatSolver("cnf/sat4j_import/hole7.cnf"));
  }

}
