#!/bin/bash

if (( "$#" )); then
    # we have a command line argument
    head="--lines=$1"
else
    # nothing specified by user, do it all
    # i.e., print everything except the last zero lines
    head="--lines=-0"
fi

./compare-cycle-counts.sh \
    `find ./cnf/sat4j_import/ -type f -name '*.cnf' | sort | head $head`
