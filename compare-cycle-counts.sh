#!/bin/bash

# print CSV header
echo "CNF,CPU_1_Nop,GPU_250_RVSIDS,GPU_250_RAssign"

# function to run solver
# 1 - engine
# 2 - initialCrewSize
# 3 - seeding strategy
# 4 - CNF file name
function miramichi() {
    ./miramichi.sh \
        -engine $1 \
        -initialCrewSize $2 \
        -seeding $3 \
        $4 \
        2>&1 \
        | gawk '/engine.cycles/ { print $NF; }' 
}

# function to run solver three times and compare results
function compare() {
    f=`basename $1`
    a=`miramichi CPU 1 NopSeedingStrategy $1`
    b=`miramichi GPU 250 RandomVariableOrderingSeedingStrategy $1`
    c=`miramichi GPU 250 RandomAssignmentSeedingStrategy $1`
    echo "$f,$a,$b,$c"
}

# run comparison for every input file named on command line
while (( "$#" )); do
    compare $1
    shift
done


