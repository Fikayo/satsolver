/**
 * @file    types.h
 * @author  Steven.Stewart@uwaterloo.ca
 */

#ifdef _MSC_VER
#define INLINE __forceinline /* use __forceinline (VC++ specific) */
#else
#define INLINE inline        /* use standard INLINE */
#endif

#ifndef types_h
#define types_h

/**
 * The lifted boolean domain covers three values: true, false, and
 * undefined. A variable can be assigned one of these three values.
 */
typedef enum lbool_t {
  lbool_false,
  lbool_true,
  lbool_undef
} lbool;

typedef int lit;  /// a literal is encoded as an integer
typedef int var;  /// a variable is encoded as an integer

#define LIT_UNDEF -2
#define VAR_UNDEF -3

// Macros for variables and literals
#define LIT_NEG(p) ((p) ^ 1)
#define LIT_SIGN(p) ((p) & 1)
#define LIT_VAR(p) ((p) >> 1)
#define LIT_INDEX(p) (p)
#define LIT_VAR_TO_LIT(v) ((v) << 1)
#define LIT_PRINT(p) LIT_SIGN(p) ? "-" : "+", (LIT_VAR(p)+1)
#define LBOOL_BOOL_TO_LBOOL(x) ( (x == 1) ? lbool_true : lbool_false )
#define VAR_PRINT(v) v+1

INLINE lbool LBOOL_NEG(lbool x) {
  if (x == lbool_true)
    return lbool_false;
  else if (x == lbool_false)
    return lbool_true;
  else
    return lbool_undef;
}

#endif // types_h
