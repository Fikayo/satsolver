/**
 * @file    VarOrder.cpp
 * @author  Steven Stewart <steven.stewart@uwaterloo.ca>
 */
#include <cassert>
#include <cstdio>
#include "VarOrder.h"

namespace Solver_Namespace {

VarOrder::VarOrder(const std::vector<lbool> &ref_to_assigns,
                   const std::vector<double> &ref_to_activity)
    : assigns_(ref_to_assigns),
      activities_(ref_to_activity) {
}

/// pre-condition: "actitivies" should be updated by solver prior to calling newVar()
void VarOrder::newVar() {
  assert(activities_.size() > 0);
  var v = (int) activities_.size() - 1;
  heap_pos_.push_back(v);
  heap_.push_back(v);
  update(v);
}

/// ensures heap properties hold (should be called after putting v at the end
/// of the heap array (heap_)
void VarOrder::update(var v) {
  assert(v >= 0 && (size_t ) v < activities_.size());
  int i = heap_pos_[v];       // index of v in heap
  var x = heap_[i];           // variable stored in position i
  int parent = (i - 1) / 2;   // index of parent of current node

  assert(i != -1);

  while (i != 0 && activities_[x] > activities_[heap_[parent]]) {
    heap_[i] = heap_[parent];
    heap_pos_[heap_[i]] = i;
    i = parent;
    parent = (i - 1) / 2;
  }
  heap_[i] = x;
  heap_pos_[x] = i;
}

void VarOrder::updateAll() {
  int nvars = (int) activities_.size();
  for (int i = 0; i < nvars; ++i)
    update(i);
}

/// puts v back onto the heap
void VarOrder::undo(var v) {
  if (heap_pos_[v] == -1) {
    heap_pos_[v] = (int) heap_.size();
    heap_.push_back(v);
    update(v);
  }
}

var VarOrder::select() {

  // Random decision:
  // @todo Implement random decision capability

  // Activity-based decision
  while (!heap_.empty()) {
    // select the next variable
    var next = heap_[0];
    heap_pos_[next] = -1;
    assert(next >= 0 && (size_t ) next < activities_.size());

    // save, then lop off the last element
    var x = heap_.back();
    heap_.pop_back();
    assert(x >= 0 && (size_t ) x < activities_.size());

    int size = (int) heap_.size();
    if (size > 0) {
      double act = activities_[x];
      int i = 0;
      int child = 1;

      // check if x belongs at i; if not, swap child up
      // into position i, and continue trickling down
      while (child < size) {
        if (child + 1 < size && activities_[heap_[child]] < activities_[heap_[child + 1]])
          child++;
        assert(child < size);

        if (act >= activities_[heap_[child]])
          break;

        heap_[i] = heap_[child];
        heap_pos_[heap_[i]] = i;
        i = child;
        child = 2 * child + 1;
      }
      heap_[i] = x;
      heap_pos_[x] = i;
    }
    if (assigns_[next] == lbool_undef)
      return next;
  }
  return VAR_UNDEF;
}

bool VarOrder::inHeap(var v) {
  return heap_pos_[v] != -1;
}

}  // Solver_Namespace
