/**
 * @file    main.cpp
 * @author  Steven.Stewart@uwaterloo.ca
 */
#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <vector>
#include "Solver.h"
#include "utilities.h"

using namespace Solver_Namespace;

int main(int argc, const char * argv[]) {
  int n;
  int m;
  Solver s;
  argc = 2;
  if (argc == 2) {
    s.readDIMACS(argv[1], false, &n, &m);
  } else {
    std::cerr << "usage: " << argv[0] << " input_cnf_file" << std::endl;
    exit(EXIT_FAILURE);
  }

  printf("\n");
  printf("------ Summary ------\n");
  printf("Number of variables:  %d\n", s.nVars());
  printf("Number of clauses:    %d\n", s.nConstraints());
  printf("\n");
  // printf("Formula:\n");
  //s.printAll();
  printf("\n");

  double iStart = cpuSecond();
  bool result = s.solve();
  double iElaps = cpuSecond() - iStart;
  printf("Time elapsed: %f seconds\n", iElaps);

  if (result) {
    printf("\n");
#ifdef DEBUG_PRINT
    //s.printAll();
#endif
    printf("\nConclusion: SAT\n");
    std::vector<bool> model = s.model();
    for (size_t i = 0; i < model.size(); ++i)
      printf("%zu=%s%s", i + 1, model[i] ? "T" : "F", i + 1 < model.size() ? " " : "");
    printf("\n");
  } else {
#ifdef DEBUG_PRINT
    //s.printAll();
#endif
    printf("Conclusion: UNSAT\n");
  }
}
