/**
 * @file    Clause.h
 * @author  Steven Stewart (steven.stewart@uwaterloo.ca)
 */

#ifndef Clause_h
#define Clause_h

#include <vector>
#include "types.h"
#include "Constr.h"

namespace Solver_Namespace {

class Solver;

/**
 * @class Clause
 * @brief A clause is a clause is a clause.
 */
class Clause : public Constr {

 public:

  int cid;                /// clause id
  bool learnt;            /// indicates a learnt clause if true
  float activity;         /// the activity value for the clause
  std::vector<lit> lits;  /// the literals of the clause

  virtual ~Clause() {
  }

  /**
   * @brief Constructor - creates a new clause and adds it to watcher lists.
   */
  static bool Clause_new(Solver &s, std::vector<lit> &ps, const bool learnt, Clause **out_clause);

  /**
   * @brief Returns whether a clause is locked (learnt clauses only).
   */
  bool locked(Solver &s);

  /**
   * @brief Removes a clause from the constraint database and watcher lists.
   */
  virtual void remove(Solver &s);

  /**
   * Carries out propagation (BCP) on the clause.
   */
  virtual bool propagate(Solver &s, const lit p);

  /**
   * @brief Simplifies the clause.
   */
  virtual bool simplify(Solver &s);

  // virtual void undo(Solver &s, const lit p);

  /**
   * @brief Calculates the "reason" for the clause.
   * @param[in]   s     A reference to the solver object.
   * @param[in]   p     (todo)
   * @param[out]  out_reason  Stores the literals for the "reason" of the clause.
   */
  virtual void calcReason(Solver &s, const lit p, std::vector<lit> &out_reason);

  /**
   * @brief Prints a representation of the clause to stdout.
   */
  void print();

};

}

#endif // Clause_h
