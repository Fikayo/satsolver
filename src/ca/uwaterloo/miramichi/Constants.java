package ca.uwaterloo.miramichi;


public class Constants {

  /** Time until execution is halted. */
  static final int TIMEOUT = 600 * 1000 * 10;

  static final int WORKER_SLEEP_MS = 0;

  static final int CONDUCTOR_SLEEP_MS = 0;

  static final double ACTIVITY_LEVEL_DECAY_RATE = 0.9;

  static final double ACTIVITY_LEVEL_INCREMENT = 1.0;

  static final int FIRST_EXPLORER_ID = 0;

  /** The number of Explorers to start out with. */
  public static final int INITIAL_CREW_SIZE = 16;

  /** System level exit codes: timeout. */
  static final int EXIT_TIMEOUT = 1;

  /** System level exit codes: exception. */
  static final int EXIT_EXCEPTION = 2;

  /** System level exit codes: computation problem. */
  static final int EXIT_BAD_COMPUTATION = 3;

  /** System level exit codes: bad input. */
  static final int EXIT_BAD_INPUT = 4;

  public enum EngineName {
    CPU, GPU1, GPU1flags, GPU1straight, GPU1packed, GPU2, GPU3, GPU4, GPU5, GPU6, GPU7
  }

  static final EngineName DEFAULT_ENGINE = EngineName.GPU6;

  public enum ExplorerName {
    MiniCDCL, Guesser, WalkSAT, Mixed
  }

  static final ExplorerName DEFAULT_EXPLORER = ExplorerName.MiniCDCL;

  static final boolean DEFAULT_GPU_PROFINING = true;

  /**
   * Below are offsets into the "block" (array) of statistics that are optionally returned as part
   * of a query result. Theses offsets are used to to look up a specific statistic of interest.
   *
   * @see QResult
   */
  public static final int STAT_BLOCK_SIZE = 5;
  public static final int STAT_OFFSET_SAT = 0;
  public static final int STAT_OFFSET_UNIT = 1;
  public static final int STAT_OFFSET_CONFL = 2;
  public static final int STAT_OFFSET_UNRES = 3;
  public static final int STAT_OFFSET_WASTE = 4;

  /** How much will we allow GPU structures to grow over the initial size of the DB? */
  public static final int CLAUSE_GROWTH_FACTOR = 200;
  public static final int LITERAL_GROWTH_FACTOR = 2000;

  public static final int DEFAULT_WORKGROUP_SIZE = 256;

  /** Width of text fields in the console log. */
  public static final int LOG_WIDTH = 30;

  /** Used to run more complex blocks of code when assertions are enabled. */
  public static boolean areAssertionsEnabled() {
    boolean x = false;
    assert x = true : "the assignment will always succeed, but will only be executed if assertions are enabled";
    return x;
  }

}
