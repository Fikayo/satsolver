package ca.uwaterloo.miramichi;

import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import ca.uwaterloo.miramichi.seeding.SeedingStrategy;

/**
 * Make random guesses and check the results (if asssertions are enabled).
 */
public class Guesser extends Explorer {

  private final int MAX_ITERATIONS = 1000;

  private final Random r;
  private int n = 0;
  public int units = 0;
  public int conflicts = 0;

  public Guesser(int eid, ClauseDB db, Assignment assigns) {
    super(eid, db, assigns);
    r = new Random(eid);
  }

  @Override
  public boolean reincarnated() {
    return false;
  }

  @Override
  public Status step() {
    logger.log(Level.FINEST, "GuesserStep: " + eid + " " + Thread.currentThread().getName());

    if (qresult != null) {

      // did we win?
      if (qresult.isSAT()) {
        System.out.println("Lucky guess!");
        // confirm the guess
        if (Constants.areAssertionsEnabled()) {
          for (final Clause c : database.clauses) {
            assert c.isSat(assigns) : "clause reported to be SAT is not.  conflict? "
                + c.isConfl(assigns) + "  unit? " + c.isUnit(assigns);
          }
        }
        return Status.SAT;

      } else if (qresult.isUnit()) {
        // confirm the units
        if (Constants.areAssertionsEnabled()) {
          for (final int clauseID : qresult.clause_ids) {
            final Clause c = database.lookup(clauseID);
            assert c.isUnit(assigns);
            units++;
          }
        }
        // confirm there are no conflicts
        if (Constants.areAssertionsEnabled()) {
          for (int i = 0; i < database.numClauses(); i++) {
            final Clause c = database.lookup(i);
            assert !c.isConfl(assigns);
          }
        }

      } else if (qresult.isConflict()) {
        // confirm the conflicts
        if (Constants.areAssertionsEnabled()) {
          for (final int clauseID : qresult.clause_ids) {
            final Clause c = database.lookup(clauseID);
            assert c.isConfl(assigns);
            conflicts++;
          }
        }

      } else if (qresult.isUnresolved()) {
        // confirm that there are no conflicts and no units
        assert qresult.clause_ids == null || qresult.clause_ids.length == 0;

      } else {
        assert false : "unknown qresult type";
      }
    }

    if (n > MAX_ITERATIONS) {
      System.out.println("Guesser reached maximum iterations. Units, conflicts: "
          + Util.i2s(units, conflicts));
      return Status.DIE;
    }

    // guess again
    switch (n++) {
      case 0:
        // all false
        for (int i = 0; i < assigns.size(); i++) {
          assigns.set(i, Assignment.FALSE);
        }
        database.query(this);
        return Status.QUERY;
      case 1:
        // all true
        for (int i = 0; i < assigns.size(); i++) {
          assigns.set(i, Assignment.TRUE);
        }
        database.query(this);
        return Status.QUERY;
      default:
        // random
        for (int i = 0; i < assigns.size(); i++) {
          // guess includes undef as a possibility
          final int value = r.nextInt(3);
          assert value <= 2;
          assigns.set(i, value);
        }
        database.query(this);
        return Status.QUERY;
    }

  }

  @Override
  public boolean setAssumptions(ArrayList<Integer> nextAssumptions) {
    return true;
  }

  private static final Logger logger = Logger.getLogger(Guesser.class.getSimpleName());


  public static class Factory extends Explorer.Factory {

    public Factory(final int crewSize, final SeedingStrategy ss) {
      super(crewSize, ss);
    }

    @Override
    public Explorer instantiateExplorer(int eid, boolean reincarnated, ClauseDB DB,
        Assignment assigns) {
      return new Guesser(eid, DB, assigns);
    }

  }
}
