package ca.uwaterloo.miramichi;

import java.util.Iterator;

public class IntArrayIterator implements Iterator<Integer> {
  private int i = 0;
  private final int[] a;

  IntArrayIterator(final int[] a) {
    this.a = a;
  }

  @Override
  public boolean hasNext() {
    return i < a.length;
  }

  @Override
  public Integer next() {
    final Integer x = a[i];
    i++;
    return x;
  }
}
