package ca.uwaterloo.miramichi;

import java.util.logging.Level;
import java.util.logging.Logger;

public class LogHelper {

  private final Logger logger;
  private final String width;
  private final boolean machinelog;

  public LogHelper(final Logger logger) {
    this(logger, 30);
  }

  public LogHelper(final Logger logger, final int width) {
    this.logger = logger;
    this.width = Integer.toString(width);
    this.machinelog = Main.machinelog_flag;
  }

  private String f(final String f) {
    return machinelog ? f.replace(":", "").replace(" ms", "") : f;
  }

  private String m(final String msg) {
    return machinelog ? msg.replace(' ', '_') : msg;
  }

  /** Log duration in milliseconds. */
  public void info(final String msg, final long n) {
    final String f = "%WIDTHs: %13.3f ms".replace("WIDTH", width);
    logger.log(Level.INFO, String.format(f(f), m(msg), n / 1e6));
  }

  public void info(final String msg, final long duration, final int cycles) {
    if (cycles == 0) {
      info(msg, duration);
    } else {
      final String f = "%WIDTHs: %13.3f ms %13.1f ms".replace("WIDTH", width);
      final float total_ms = (float) (duration / 1e6);
      final float cycle_ms = total_ms / cycles;
      logger.log(Level.INFO, String.format(f(f), m(msg), total_ms, cycle_ms));
    }
  }

  /** Log a count. */
  public void info(final String msg, final int n) {
    final String f = "%WIDTHs: %9d".replace("WIDTH", width);
    logger.log(Level.INFO, String.format(f(f), m(msg), n));
  }

  /** Log a string. */
  public void info(final String msg, final String n) {
    final String f = "%WIDTHs: %s".replace("WIDTH", width);
    logger.log(Level.INFO, String.format(f(f), m(msg), n));
  }

  /** Log a boolean. */
  public void info(final String msg, final Boolean n) {
    final String f = "%WIDTHs: %b".replace("WIDTH", width);
    logger.log(Level.INFO, String.format(f(f), m(msg), n));
  }

  /** Log a float. */
  public void info(final String msg, final Float n) {
    final String f = "%WIDTHs: %9.0f".replace("WIDTH", width);
    logger.log(Level.INFO, String.format(f(f), m(msg), n));
  }

}
