package ca.uwaterloo.miramichi;

/**
 * A collection of static convenience methods. Not to be instantiated.
 */
public final class Literal {

  private Literal() {
    throw new UnsupportedOperationException("just static convenience methods");
  }

  public final static int neg(final int lit) {
    return (lit) ^ 1;
  }

  public final static int intsign(final int lit) {
    return ((lit) & 1);
  }

  public final static boolean sign(final int lit) {
    return ((lit) & 1) == 0;
  }

  public final static int var(final int lit) {
    return (lit) >> 1;
  }

  public final static int var2lit(final int var) {
    return (var) << 1;
  }

  public final static String toString(final int lit) {
    return (sign(lit) ? "+" : "-") + (var(lit) + 1);
  }

}
