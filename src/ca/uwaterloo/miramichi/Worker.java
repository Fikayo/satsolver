package ca.uwaterloo.miramichi;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * One worker per CPU core (roughly). All workers share a common queue of Explorers. The Conductor
 * and all Workers share the same explorer queue. The Conductor puts explorers on and the workers
 * take them off.
 */
public final class Worker implements Runnable {

  private static final Logger logger = Logger.getLogger(Worker.class.getSimpleName());

  /**
   * A queue of explorers ready to be put to work. Shared between all workers and the conductor. The
   * conductor adds explorers to the queue and the workers remove them.
   *
   * @see Conductor#pending
   */
  private final BlockingQueue<Explorer> pending;

  /** The ID number of this Worker, for debugging messages. */
  private final int workerID;

  /**
   * Indicates the presence of work to be processed. Shared by all workers and the conductor. Once
   * set to false then workers will terminate; if more workers are desired after this point then
   * they will need to be created.
   *
   * @see Conductor#work
   */
  private final AtomicBoolean work;
  private final AtomicBoolean exit;

  /**
   * A reference to a CyclicBarrier shared by all Workers, used to enforce distinct CPU and GPU
   * phases of computation.
   */
  private final CyclicBarrier barrier;

  /** Explorer Factory for reincarnating explorers. */
  private final Explorer.Factory explorerFactory;

  /**
   * Constructs a new Worker.
   *
   * @param pendingQueue a reference to the shared queue of explorers
   * @param work a reference to the shared flag indicating the presence of work
   */
  public Worker(final int workerID, final BlockingQueue<Explorer> pendingQueue,
      final AtomicBoolean work, final AtomicBoolean exit, final CyclicBarrier barrier,
      final Explorer.Factory explorerFactory) {
    this.workerID = workerID;
    this.pending = pendingQueue;
    this.work = work;
    this.exit = exit;
    this.barrier = barrier;
    this.explorerFactory = explorerFactory;
    assert workerID >= 0;
  }

  /**
   * The worker thread polls the pending queue until an explorer becomes available. The
   * {@link Conductor conductor}, which runs in its own thread, places explorers into the pending
   * queue, making them immediately available for a worker thread to grab.
   * <p>
   * After grabbing the next available explorer, the worker thread calls the explorer's step method
   * to advance the explorer in its search. The result of the call to step is then inspected. If it
   * is SAT or UNSAT, then the result is communicated to the main thread. Note that it is not the
   * responsibility of the worker thread to return the explorer to the pending queue -- the
   * conductor automatically takes care of this.
   * </p>
   *
   * @see Explorer#step()
   * @see Main#result
   */
  @Override
  public void run() {
    while (!this.exit.get()) {
      try {
        final Explorer expl = pending.poll(0, TimeUnit.SECONDS); // non-blocking call
        if (expl == null) {
          // Queue is empty, for one of two reasons:
          // 1. Problem solved, no more work to do.
          // 2. Race condition.
          barrier.await();
        } else {
          // process this explorer
          final Explorer.Status status = expl.step();
          if (status.equals(Explorer.Status.SAT)) {
            boolean old_work = work.getAndSet(false);
            if (old_work) {
              Main.result = Main.Result.SAT;
              logger.log(Level.INFO, "SAT!");
              logger.log(Level.FINE, () -> expl.assigns.toString());
            }
          } else if (status.equals(Explorer.Status.UNSAT)) {
            boolean old_work = work.getAndSet(false);
            if (old_work) {
              Main.result = Main.Result.UNSAT;
              logger.log(Level.INFO, "UNSAT!");
            }
          } else if (status.equals(Explorer.Status.REINCARNATE)) {
            // TODO use explorer factory for reincarnation
            // TODO reincarnate should be seeded by RandomVariableOrderingSeedingStrategy
            // TODO reincarnate should be seeded by setting VSIDS for variables that were
            // conflicting in assumptions
            final Explorer expl2 = explorerFactory.reincarnate(expl);
            assert expl2.reincarnated();
            final Explorer.Status status2 = expl2.step();
            if (!status2.equals(Explorer.Status.QUERY)) {
              // something has gone wrong ... first step should always be QUERY
              boolean old_work = work.getAndSet(false);
              if (old_work) {
                logger.log(Level.INFO, "Killing, bad reincarnation!");
              }
            }
          } else if (status.equals(Explorer.Status.DIE)) {
            boolean old_work = work.getAndSet(false);
            if (old_work) {
              logger.log(Level.INFO, "Killing, bad explorer state!");
            }
          }
        }
        // we do not put e back on the pending queue here
        // the Conductor will do that when his Train comes in

        // Wait here for all other workers.

        // Restart loop: check work flag and queue.
        // If problem is solved, work flag will be false and we will exit.
      } catch (Throwable e) {
        work.set(false);
        e.printStackTrace();
        try {
          barrier.await();
        } catch (Exception ex) {
          throw new RuntimeException();
        }
        throw new RuntimeException();
      }
    }
    // after while loop
  }
}
