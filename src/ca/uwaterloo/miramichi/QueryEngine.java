package ca.uwaterloo.miramichi;

import java.util.List;
import java.util.logging.Logger;

/**
 * A QueryEngine processes a SAT solver's queries. It includes a reference to a centrally-managed
 * clause database, called DB, as well as specifying methods for computing a query and consolidating
 * (synchronizing) the engine's encoding of the clause database with that of the centrally-managed
 * clause database.
 */
public abstract class QueryEngine {

  /** Logger. */
  private static final Logger logger = Logger.getLogger(QueryEngine.class.getName());

  /** A reference to the centrally-managed, shared clause database (DB). */
  protected final ClauseDB DB;

  /**
   * Engine statistics.
   */
  protected final EngineStats engineStats = new EngineStats();

  public static final class EngineStats {
    public int cycles = 0;
    public long consolidation_duration = 0;
    public long processing_duration = 0;
  }

  /**
   * Constructs a QueryEngine with the specified clause database. Note that the clause database must
   * not be null.
   *
   * @param DB the centrally-managed, shared clause database.
   */
  public QueryEngine(final ClauseDB DB) {
    this.DB = DB;
    assert DB != null;
  }

  /**
   * Compute the results for the specified queries. This must be a blocking call: it must not return
   * until all results have been computed; moreover, this method is responsible for communicating
   * the results back to the explorers who initiated the queries.
   *
   * @param explorers
   */
  public final void compute(final List<Explorer> explorers) {
    long engineComputeStartTime = System.nanoTime();
    computeInner(explorers);
    long engineComputeEndTime = System.nanoTime();
    engineStats.processing_duration += (engineComputeEndTime - engineComputeStartTime);
    engineStats.cycles++;
  }

  protected abstract void computeInner(final List<Explorer> explorers);

  /**
   * A specialization of a {@link QueryEngine query engine} needs to implement a Factory class with
   * a newEngine method as part of a creational design pattern (i.e., a concrete engine instance is
   * created by calling newEngine on the factory object).
   */
  public static abstract class Factory {
    public abstract QueryEngine newEngine(final ClauseDB DB);
  }

  /**
   * The database has been updated. If it needs to be replicated elsewhere (e.g., GPU), do so now.
   * In other words, this will ensure that the engine's version of the database is synchronized with
   * that of the centrally-managed database (DB).
   *
   * @param learntClauses The clauses that have recently been learnt (returned from
   *        ClauseDB.consolidate()).
   * @see ClauseDB#consolidate()
   */

  public final void consolidateDB(List<Clause> learntClauses) {
    long engineConsolidateStartTime = System.nanoTime();
    consolidateDBInner(learntClauses);
    long engineConsolidateEndTime = System.nanoTime();
    engineStats.consolidation_duration += (engineConsolidateEndTime - engineConsolidateStartTime);
  }

  protected abstract void consolidateDBInner(List<Clause> learntClauses);

  /**
   * Release any machine resources that were used by this engine, and log performance statistics.
   */
  public final EngineStats releaseResources() {
    releaseResourcesInner();
    return engineStats;
  }

  protected abstract void releaseResourcesInner();
}
