package ca.uwaterloo.miramichi;

import java.util.BitSet;

/**
 * An assignment of true/false/undefined to variables. Variables are identified by position.
 *
 * The expected use is that there is one true Assignment, and each explorer has a reference to a
 * SubAssignment that restricts its view of the Assignment to the indices related to it.
 *
 * The set(int,int) method and the clearDirtyVars() methods are synchronized, but no others are.
 * This is a precaution in case the number of variables in the formula is not aligned with 4. If the
 * number of variables were aligned with 4, then no two explorers would ever access the same index
 * into the underlying array and we would not need to synchronize.
 *
 * Possible locking reduction: allocate an array of locks with one cell for each cell in the
 * underlying byte array. Keep cells empty, except for cells that are shared by two adjacent
 * explorers. Lock on the lock, if present.
 */
public class Assignment {

  public static final int TRUE = 0;
  public static final int FALSE = 1;
  public static final int UNDEF = 2;

  private final BitSet dirtyVars;

  /**
   * The only valid values are 0 (True), 1 (False), and 2 (Undefined).
   */
  private static final int MAX_VALUE = UNDEF;

  /**
   * We need two bits to encode each assignment, since there are three possible values: true, false,
   * and undefined.
   */
  private static final int BITS_PER_ASSIGNMENT = 2;

  /**
   * The number of assignments that will fit in each cell of the underlying array. For now we use a
   * byte array, where each byte is 8 bits, so we get four assignments per cell.
   */
  public static final int ASSIGNMENTS_PER_CELL = Byte.SIZE / BITS_PER_ASSIGNMENT;

  /**
   * The array that encodes the assignment. Not intended to be indexed by clients. Accessible so
   * that it can be uploaded in bulk to the GPU.
   */
  public final byte[] a;

  protected final int size;

  public Assignment(final int size) {
    a = new byte[cellCount(size)];
    this.size = size;
    this.dirtyVars = new BitSet(size);
    assert size <= cellCount(size) * ASSIGNMENTS_PER_CELL;
  }

  protected Assignment(final byte[] a2, final BitSet dirtyVars2, final int size2) {
    this.a = a2;
    this.size = size2;
    this.dirtyVars = dirtyVars2;
    assert size <= cellCount(size) * ASSIGNMENTS_PER_CELL;
  }

  public int get(int index) {
    return get(offset(index), remainder(index));
  }

  public final int get(final int offset, final int remainder) {
    return (a[offset] >>> remainder) & 0x3;
  }

  public final boolean isTrue(final int index) {
    return get(index) == TRUE;
  }

  public final boolean isFalse(final int index) {
    return get(index) == FALSE;
  }

  public final boolean isUndef(final int index) {
    return get(index) == UNDEF;
  }

  public final boolean isTrue(final int offset, final int remainder) {
    return get(offset, remainder) == TRUE;
  }

  public final boolean isFalse(final int offset, final int remainder) {
    return get(offset, remainder) == FALSE;
  }

  public final boolean isUndef(final int offset, final int remainder) {
    return get(offset, remainder) == UNDEF;
  }

  public void set(final int index, final int value) {
    assert value <= MAX_VALUE;
    assert index < size;
    final int offset = offset(index);
    final int remainder = remainder(index);
    set(offset, remainder, value);
  }

  public final void set(final int offset, final int remainder, final int value) {
    assert value <= MAX_VALUE;
    // synchronize on the underlying array rather than this, because it might be shared
    synchronized (a) {
      // set the variable
      a[offset] &= ~(0x3 << remainder);
      a[offset] |= value << remainder;
      // add this variable to the dirtyVars
      final int index = index(offset, remainder);
      assert index <= size : "error in computation of var index: " + Util.i2s(index, size);
      dirtyVars.set(index);
    }
  }

  public final void clearDirtyVars() {
    synchronized (a) {
      dirtyVars.clear();
    }
  }

  public int size() {
    return size;
  }

  @Override
  public final String toString() {
    final StringBuffer b = new StringBuffer(size * 4);
    b.append('[');
    for (int i = 0; i < size - 1; i++) {
      b.append(toString(get(i)));
      b.append(", ");
    }
    b.append(toString(get(size - 1)));
    b.append(']');
    return b.toString();
  }

  public static String toString(final int value) {
    switch (value) {
      case TRUE:
        return "T";
      case FALSE:
        return "F";
      case UNDEF:
        return "?";
      default:
        throw new IllegalStateException("unknown assignment value: " + value);
    }
  }

  protected static int offset(final int index) {
    return index >>> 2;
  }

  protected static int remainder(final int index) {
    return (index & 0x3) << 1;
  }

  /** Inverse of offset and remainder functions. */
  protected static int index(final int offset, final int remainder) {
    return (offset * ASSIGNMENTS_PER_CELL) + (remainder >>> 1);
  }

  public static int cellCount(int size) {
    return (int) Math.ceil((float) size / (float) ASSIGNMENTS_PER_CELL);
  }

  public static int cellCount(final int num_explorers, final int num_vars) {
    final int cells_per_explorer = cellCount(num_vars);
    return num_explorers * cells_per_explorer;
  }


  /** Number of cells used by this Sub-Assignment. */
  public final int cellCount() {
    return cellCount(size());
  }

  /** Number of cells used by all Sub-Assignments. */
  public final int globalCellCount() {
    return cellCount(size);
  }

  public Assignment subAssignment(final int startIndex, final int length) {
    assert length <= size;
    assert startIndex + length <= size : Util.i2s(startIndex, length, size);
    return new SubAssignment(this, startIndex, length);
  }

  /**
   * A SubAssignment presents a range-restricted view of an underlying Assignment, with which it
   * shares (aliases) the array and the dirtyVars BitSet.
   */
  private static class SubAssignment extends Assignment {

    private final int startIndex;

    private final int self_size;

    public SubAssignment(final Assignment assignment, final int startIndex, final int size) {
      super(assignment.a, assignment.dirtyVars, assignment.size);
      this.startIndex = startIndex;
      this.self_size = size;
    }

    @Override
    public int size() {
      return self_size;
    }

    @Override
    public int get(final int index) {
      assert index < self_size;
      // index into the master Assignment
      return super.get(startIndex + index);
    }

    @Override
    public void set(final int index, final int value) {
      assert index < self_size;
      // index into the master Assignment
      super.set(startIndex + index, value);
    }

    @Override
    public Assignment subAssignment(final int startIndex2, final int length) {
      return super.subAssignment(startIndex + startIndex2, length);
    }

  }

  /**
   * Returns a shared reference that should not be modified by clients.
   */
  public BitSet getDirtyVars() {
    return dirtyVars;
  }
}
