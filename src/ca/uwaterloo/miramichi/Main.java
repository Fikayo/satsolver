package ca.uwaterloo.miramichi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;

import ca.uwaterloo.miramichi.gpu.AbstractGPUEngine;
import ca.uwaterloo.miramichi.gpu.GPUEngineFactory;
import ca.uwaterloo.miramichi.seeding.SeedingStrategy;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;

/**
 * The program starting point.
 */
public final class Main {

  @Parameter(description = "DIMACS CNF Files to solve")
  private List<String> files = new ArrayList<String>();

  @Parameter(names = "-initialCrewSize", description = "Number of explorers to start with")
  private Integer initialCrewSize = Constants.INITIAL_CREW_SIZE;

  @Parameter(names = "-multiFactor",
      description = "Maximum number of clauses to return per explorer per engine cycle")
  private Integer multiFactor = 11;

  @Parameter(names = "-activePagesOnly",
      description = "Compute only for pages on which variables have changed")
  private Boolean activePagesOnly = false;

  @Parameter(names = "-explorer",
      description = "Name of Explorer to use: Mixed, MiniCDCL, Guesser, WalkSAT")
  private String explorerName = Constants.DEFAULT_EXPLORER.toString();
  private Explorer.Factory explorerFactory = null;

  @Parameter(names = "-engine", description = "Name of QueryEngine to use")
  private String engineName = Constants.DEFAULT_ENGINE.toString();
  private QueryEngine.Factory engineFactory = null;

  @Parameter(names = "-gpuprof", description = "Enable GPU profiling")
  private Boolean gpuprof = Constants.DEFAULT_GPU_PROFINING;

  @Parameter(names = "-align", description = "Align clauses to GPU workgroup boundaries")
  private Boolean alignDB = Constants.DEFAULT_ENGINE.ordinal() >= Constants.EngineName.GPU3
      .ordinal();

  @Parameter(names = "-seeding", description = "Name of SeedingStrategy to use")
  private String seedingStrategyName = "RandomAssignmentSeedingStrategy";
  private SeedingStrategy seedingStrategy = null;

  @Parameter(names = "-machinelog",
      description = "Make log messages more conducive to machine processing")
  private Boolean machinelog_param = false;
  /** This static will be set based on the command line parameter. */
  public static boolean machinelog_flag = false;

  private ClauseDB DB = null;

  private static final Logger logger = Logger.getLogger(Main.class.getSimpleName());

  public enum Result {
    SAT {
      @Override
      public boolean toBoolean() {
        return true;
      }
    },
    UNSAT {
      @Override
      public boolean toBoolean() {
        return false;
      }
    };

    public abstract boolean toBoolean();
  }

  public static volatile Result result = null;

  private Main() {
    // needed by JCommander for argument processing
  }

  public Main(final ClauseDB db, final Explorer.Factory efactory, final QueryEngine.Factory qfactory) {
    this.DB = db;
    this.explorerFactory = efactory;
    this.engineFactory = qfactory;
  }

  public static void main(final String[] args) {
    // process args
    final Main main = new Main();
    final JCommander jc = new JCommander(main, args);
    main.processArgs(jc);
    // compute
    main.main();
    // done.
    logger.log(Level.FINEST, "Terminating.");
  }

  private void badArgs(final JCommander jc, final String msg) {
    System.err.println(msg);
    jc.setProgramName(getClass().getName());
    jc.usage();
    System.exit(Constants.EXIT_BAD_INPUT);
  }

  private void processArgs(final JCommander jc) {

    machinelog_flag = machinelog_param;

    // clause database
    if (files.size() == 0) {
      final String msg = "Missing: DIMACS file name.";
      badArgs(jc, msg);
    }

    // initial crew size
    logger.log(Level.CONFIG, "InitialCrewSize = " + initialCrewSize);

    // seeding strategy
    if (!seedingStrategyName.startsWith("ca.uwaterloo")) {
      seedingStrategyName = "ca.uwaterloo.miramichi.seeding." + seedingStrategyName;
    }
    try {
      final Class<?> c = Class.forName(seedingStrategyName);
      seedingStrategy = (SeedingStrategy) c.newInstance();
    } catch (final Exception e) {
      throw new RuntimeException(e);
    }
    logger.log(Level.CONFIG, "SeedingStrategy = " + seedingStrategy.getClass());

    // explorer factory
    if ("MiniCDCL".equals(explorerName)) {
      explorerFactory = new MiniCDCL.Factory(initialCrewSize, seedingStrategy);;
    } else if ("Guesser".equals(explorerName)) {
      explorerFactory = new Guesser.Factory(initialCrewSize, seedingStrategy);
    } else if ("WalkSAT".equals(explorerName)) {
      explorerFactory = new WalkSAT.Factory(initialCrewSize, seedingStrategy);
    } else if ("Mixed".equals(explorerName)) {
      explorerFactory = new MixedExplorerFactory(initialCrewSize, seedingStrategy);
    } else {
      final String msg = "Explorer not understood: " + explorerName;
      badArgs(jc, msg);
    }
    logger.log(Level.CONFIG, "ExplorerFactory = " + explorerFactory.getClass());

    // query engine
    final int workgroupSize;
    if ("CPU".equals(engineName)) {
      engineFactory = CPUEngine.FACTORY;
      workgroupSize = Constants.DEFAULT_WORKGROUP_SIZE;
      if (alignDB) {
        System.err.println("Clause alignment is not necessary with CPU engine.");
      }
    } else if (engineName.startsWith("GPU")) {
      final GPUEngineFactory gpuEngineFactory = new GPUEngineFactory(engineName, initialCrewSize);
      engineFactory = gpuEngineFactory;
      workgroupSize = gpuEngineFactory.workgroupSize();
    } else {
      final String msg =
          "Did not understand requested engine name. Try one of: "
              + Arrays.toString(Constants.EngineName.values()) + " -- " + engineName;
      badArgs(jc, msg);
      workgroupSize = Constants.DEFAULT_WORKGROUP_SIZE;
    }
    AbstractGPUEngine.GPU_PROFILING = gpuprof;
    AbstractGPUEngine.MULTI_FACTOR = multiFactor;
    AbstractGPUEngine.activePagesOnly = activePagesOnly;
    logger.log(Level.CONFIG, "MultiFactor = " + multiFactor);
    logger.log(Level.CONFIG, "EngineFactory = " + engineFactory.getClass());

    // load CNF file
    DB = ClauseDB.FromCNFFilePath(files.get(0), alignDB, workgroupSize);
    logger.log(Level.CONFIG, "DIMACS = " + files.get(0));

    // done.
  }

  public Result main() {
    result = null;
    try {

      // flag to indicate if work needs to be done
      // shared by all Workers and Conductor
      // @see Worker#work
      // @see Conductor#work
      final AtomicBoolean work = new AtomicBoolean(true);

      // flag to indicate if the solver should exit
      // shared by all Workers and Conductor
      // @see Worker#exit
      // @see Conductor#exit
      final AtomicBoolean exit = new AtomicBoolean(false);

      // queue of explorers waiting to take their next step
      // shared by all Workers and Conductor
      // @see Worker#pending
      // @see Conductor#pending
      BlockingQueue<Explorer> pending = new LinkedBlockingQueue<Explorer>();

      // create assignment array to be shared by all explorers
      final int nv = DB.varCount();
      final int nv_aligned = Assignment.cellCount(nv) * Assignment.ASSIGNMENTS_PER_CELL;
      final Assignment assigns = new Assignment(explorerFactory.initialCrewSize * nv_aligned);

      // create explorers
      Explorer.nextID.set(Constants.FIRST_EXPLORER_ID);
      // explorers should be aligned to assignment cell boundaries
      for (int i = 0; i < explorerFactory.initialCrewSize; i++) {
        final int startIndex = nv_aligned * i;
        final Explorer ex = explorerFactory.newExplorer(DB, assigns.subAssignment(startIndex, nv));
        pending.put(ex);
      }

      // create conductor thread
      final QueryEngine engine = engineFactory.newEngine(DB);
      final Conductor conductor = new Conductor(engine, pending, work, exit, DB);
      DB.setConductor(conductor);

      // figure out how many workers
      final int processorCount = Runtime.getRuntime().availableProcessors();
      final int workerCount = processorCount;
      logger.log(Level.CONFIG, "Number of workers: " + workerCount);

      // cyclic barrier to enforce CPU and GPU phases of computation
      // the CyclicBarrier will call the conductor when the workers are waiting
      final CyclicBarrier barrier = new CyclicBarrier(workerCount, conductor);

      // start the clock
      final long startTime = System.nanoTime();

      // create worker threads
      final Set<Thread> worker_threads = new HashSet<Thread>();
      for (int i = 0; i < workerCount; i++) {
        final Thread t =
            new Thread(new Worker(i, pending, work, exit, barrier, explorerFactory), "Worker" + i);
        t.start();
        worker_threads.add(t);
      }

      // global timeout: kill the program if it takes too long
      Timer timer = new Timer();
      timer.schedule(new TimerTask() {
        @Override
        public void run() {
          work.set(false);
          logger.log(Level.SEVERE, "Timeout - terminating.");
          // release machine resources used by the engine
          engine.releaseResources();
          System.exit(Constants.EXIT_TIMEOUT);
        }
      }, Constants.TIMEOUT);

      // Wait for threads to terminate before exiting program.
      for (Thread thread : worker_threads) {
        thread.join();
      }
      timer.cancel();

      // stop the clock
      final long endTime = System.nanoTime();

      // release machine resources used by the engine
      final QueryEngine.EngineStats es = engine.releaseResources();

      // log performance statistics
      final long duration = (endTime - startTime);
      final long conductorDuration =
          (duration - es.consolidation_duration - es.processing_duration);
      final LogHelper L = new LogHelper(logger, 30);
      L.info("assertions enabled", Constants.areAssertionsEnabled());
      L.info("gpu profiling", gpuprof);
      L.info("query engine", engineName);
      L.info("seeding strategy", seedingStrategy.getClass().getSimpleName());
      L.info("initial crew size", initialCrewSize);
      L.info("worker count", workerCount);
      L.info("engine cycles", es.cycles);
      L.info("conductor duration", conductorDuration, es.cycles);
      L.info("engine processing duration", es.processing_duration, es.cycles);
      L.info("engine consolidation duration", es.consolidation_duration, es.cycles);
      L.info("total duration", duration, es.cycles);
      final int d = es.cycles * initialCrewSize;
      final long n = (d == 0) ? 0 : es.processing_duration / d;
      L.info("engine time per explorer query", n);

      // log DB stats
      DB.logstats();

      assert result != null : "solver result should not be null";
      return result;

    } catch (final Exception e) {
      // something went wrong ...
      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }

}
