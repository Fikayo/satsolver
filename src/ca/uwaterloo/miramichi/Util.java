package ca.uwaterloo.miramichi;

import java.util.List;

public class Util {

  private Util() {
    throw new UnsupportedOperationException("static utility methods only");
  }

  public static String i2s(final Integer... integers) {
    final StringBuilder b = new StringBuilder();
    for (final Integer i : integers) {
      b.append(i);
      b.append(' ');
    }
    return b.toString();
  }

  public static int[] list2array(final List<Integer> list) {
    if (list == null) {
      return new int[0];
    }
    final int[] result = new int[list.size()];
    int i = 0;
    for (final Integer x : list) {
      result[i] = x;
      i++;
    }
    return result;
  }
}
