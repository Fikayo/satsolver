package ca.uwaterloo.miramichi;

import ca.uwaterloo.miramichi.Explorer.Factory;
import ca.uwaterloo.miramichi.seeding.SeedingStrategy;

public class MixedExplorerFactory extends Factory {

  private final Explorer.Factory miniFactory;
  private final Explorer.Factory guesserFactory;

  public MixedExplorerFactory(final int crewSize, final SeedingStrategy ss) {
    super(crewSize, ss);
    miniFactory = new MiniCDCL.Factory(crewSize, ss);
    guesserFactory = new Guesser.Factory(crewSize, ss);
  }

  @Override
  public Explorer instantiateExplorer(int eid, boolean reincarnated, ClauseDB DB, Assignment assigns) {
    if (initialCrewSize > 1 && eid == 0) {
      // Guesser
      return guesserFactory.instantiateExplorer(eid, reincarnated, DB, assigns);
    } else {
      // MiniCDCL
      return miniFactory.instantiateExplorer(eid, reincarnated, DB, assigns);
    }
  }
}
