package ca.uwaterloo.miramichi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.SortedMap;

/**
 * An immutable clause object.
 */
public final class Clause {

  public final static int SAT = 0;
  public final static int CONFLICT = 1;
  public final static int UNIT = 2;
  public final static int WASTE = 3;
  public final static int UNRES = 4;

  /** Value when database pages are not being used. */
  public final static int NO_PAGE = -1;

  public final int cid;
  private final int[] lits;
  public final int page;

  @Override
  public int hashCode() {
    return Arrays.hashCode(lits);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Clause other = (Clause) obj;
    if (!Arrays.equals(lits, other.lits))
      return false;
    return true;
  }

  /** Should only be called by ClauseDB. */
  Clause(final int cid, final List<Integer> litlist) {
    this(cid, NO_PAGE, litlist.iterator(), litlist.size());
  }

  Clause(final int cid, final int page, final Iterator<Integer> it, final int length) {
    this.cid = cid;
    this.page = page;
    assert length > 0;
    // copy contents of litlist into a new lits array
    this.lits = new int[length];
    // copy in real values
    int i = 0;
    while (it.hasNext()) {
      assert i < length;
      final Integer x = it.next();
      lits[i] = x;
      i++;
    }
  }

  /**
   * Should only be called by ClauseDB. Aliases the lits of the original clause. Gives the new
   * clause the new requested ID.
   */
  Clause(final int cid, final int page, final Clause original) {
    this.cid = cid;
    this.page = page;
    this.lits = original.lits;
  }

  private Clause(final int cid, final int page, final int[] lits) {
    this.cid = cid;
    this.page = page;
    this.lits = lits;
  }

  public int size() {
    return lits.length;
  }

  public int lit(final int i) {
    return lits[i];
  }

  /**
   *
   * @return The state of the clause under the given variable assignments.
   */
  public int clauseState(final Assignment assigns) {
    // Count the number of unassigned literals; if one of them is assigned and satisfied, return SAT
    byte count = 0;
    for (int p : lits) {
      final int a = assigns.get(Literal.var(p));
      if (a == Assignment.UNDEF) {
        ++count;
      } else if (a == Literal.intsign(p)) {
        return SAT;
      }
    }

    // Return clause state based on count
    if (count == lits.length) {
      return WASTE;
    } else if (count == 0) {
      return CONFLICT;
    } else if (count == 1) {
      return UNIT;
    } else {
      return UNRES;
    }
  }

  /** Returns whether this clause is unit under the variable assignment. **/
  public boolean isUnit(final Assignment assigns) {
    final int x = getUnitLiteral(assigns);
    return x >= 0;
  }

  /** If the clause is unit, returns the unit literal; otherwise, returns -1. */
  public int getUnitLiteral(final Assignment assigns) {
    int unitLiteral = -1;
    int count = 0;
    for (int i = 0; i < lits.length && count < 2; i++) {
      if (assigns.isUndef(Literal.var(lits[i]))) {
        ++count;
        unitLiteral = lits[i];
      }
    }
    return (count == 1) ? unitLiteral : -1;
  }

  /** Returns if this clause is satisfied under the variable assignment. **/
  public boolean isSat(final Assignment assigns) {
    for (int p : lits) {
      // Checks if the sign of the assignment is the same as the sign of the literal.
      if (assigns.get(Literal.var(p)) == Literal.intsign(p))
        return true;
    }
    return false;
  }

  /** Returns satisfied literals, or an empty list if none. **/
  public List<Integer> getSatLiterals(final Assignment assigns) {
    final ArrayList<Integer> a = new ArrayList<>();
    for (int p : lits) {
      // Checks if the sign of the assignment is the same as the sign of the literal.
      if (assigns.get(Literal.var(p)) == Literal.intsign(p))
        a.add(p);
    }
    return a;
  }

  /** Returns if this clause is conflicting under the variable assignment. **/
  public boolean isConfl(final Assignment assigns) {
    for (int p : lits) {
      final int var = Literal.var(p);
      final int actual = assigns.get(var);
      final int expected = Literal.intsign(p) ^ 1;
      if (actual != expected) {
        final int e2_int = Literal.neg(Literal.intsign(p));
        // this method is only ever called from assertions
        // this case is never desired, always an error
        System.err.println("lit: " + p + "  v_int: " + var + "  a_int: " + actual + "  e_int: "
            + expected + "  e2_int: " + e2_int + "\nlits: " + Arrays.toString(lits));
        return false;
      }
    }
    return true;
  }

  public final static Comparator<Clause> ID_COMPARATOR = new IDComparator();

  private final static class IDComparator implements Comparator<Clause> {

    @Override
    public int compare(final Clause c1, final Clause c2) {
      if (c1.cid < c2.cid)
        return -1;
      else if (c1.cid > c2.cid)
        return 1;
      else
        return 0;
    }

  }

  @Override
  public String toString() {
    StringBuilder result = new StringBuilder();
    if (cid >= 0) {
      result.append(cid);
      result.append(":");
      result.append("{");
      result.append(page);
      result.append("}");
    }
    result.append("(");
    for (int i = 0; i < lits.length; i++) {
      result.append(Literal.toString(lits[i]));
      if (i + 1 < lits.length)
        result.append(" ");
    }
    result.append(")");
    return result.toString();
  }

  /** Return a new clause with variables remapped according to the parameter. */
  public Clause varRemap(final SortedMap<Integer, Integer> remap) {
    final int[] lits2 = new int[lits.length];
    boolean remapped = false;
    for (int i = 0; i < lits.length; i++) {
      final int lit = lits[i];
      final int var = Literal.var(lit);
      final Integer var2 = remap.get(var);
      if (var2 == null) {
        // no remapping
        lits2[i] = lit;
      } else {
        // remapping
        final int lit2 =
            Literal.sign(lit) ? Literal.var2lit(var2) : Literal.neg(Literal.var2lit(var2));
        lits2[i] = lit2;
        remapped = true;
      }
    }
    final Clause c = new Clause(cid, page, lits2);
    if (Constants.areAssertionsEnabled()) {
      if (remapped) {
        System.out.println(this + " --> " + c);
      }
    }
    return c;
  }

}
