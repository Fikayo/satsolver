// fill an array of integers with VALUE
__kernel void array_fill_int_unroll(__global int* const a, const int length)
{
    const int wid = get_group_id(0);
    const int lid = get_local_id(0);
    const int base = UNROLL * STEP * wid + lid;

    #pragma unroll UNROLL
    for (int i=0, j = base; i < UNROLL; i++, j += STEP) {
        a[j] = VALUE;
    }


/*    
    const int tid = get_global_id(0);
    #pragma unroll UNROLL
    for (int i = tid; i < length; i += STEP) {
        a[i] = VALUE;
    }
*/    
//	const int i = tid;
//	a[i] = VALUE;
//	a[i+1*STEP] = VALUE;
//	a[i+2*STEP] = VALUE;
//	a[i+3*STEP] = VALUE;
}
