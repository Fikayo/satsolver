// Macros for literals
#define LIT_SIGN(p) ((p) & 1)
#define LIT_VAR(p) ((p) >> 1)

// Lifted Boolean domain
#define LBOOL_TRUE 0
#define LBOOL_FALSE 1
#define LBOOL_UNDEF 2

// Statistics
#define STAT_BLOCK_SIZE 5
#define STAT_OFFSET_SAT 0
#define STAT_OFFSET_UNIT 1
#define STAT_OFFSET_CONFL 2
#define STAT_OFFSET_UNRES 3
#define STAT_OFFSET_WASTE 4


// Inspect one literal for one explorer
__kernel void gpu5_loop(
    // inputs
    const int num_explorers,
    const int num_variables,
    const int num_literals,
    const int num_clauses,
    __global const char* const assignments,     // size == num_explorers * num_variables
    __global const int*  const literals,        // size == sum of length of all clauses
    __global const int*  const lit2clause,      // size == same as literals[]
    __global const int*  const clause_lengths,  // size == number of clauses
    // MULTI_FACTOR is set by the pre-processor, not an argument
    // UNROLL is set by the pre-processor, not an argument
    // outputs
    // initialized to zeroes
    __global int* const units,      // size == num_explorers * MULTI_FACTOR
    __global int* const conflicts,  // size == num_explorers * MULTI_FACTOR
    // initialized to zeroes
    __global int* const stats       // size == num_explorer * STAT_BLOCK_SIZE

    ) {

  // one cell for each clause being processed in this workgroup
  // the maximum number of clauses that could be processed is the
  // number of literals
  __local int num_lit_undefs_local[LITS_PER_WORKGROUP];

  // decode work item
  // first dimension is explorer
  const int explorerID = get_global_id(0);
  // second dimension is literals
  const int groupID = get_group_id(1);
  const int threadID = get_local_id(1);
  const int literalID = get_global_id(1);

  // the first literal in this workgroup and thread
  const int group_base = groupID * LITS_PER_WORKGROUP;
  const int thread_base = group_base + threadID;


  // clause IDs to remember
  int unitClauseID = -1;
  int conflictClauseID = -1;

  #pragma unroll UNROLL
  for (int loop=0, literalID = thread_base; loop < UNROLL; loop++, literalID += WORKGROUPSIZE) {
  
    // literalID might be greater than num_literals due to workgroup sizing
    const int inWorkgroup = (literalID < num_literals);

    if (inWorkgroup) {
      // decode clauseID
      const int clauseID = lit2clause[literalID];
      const int localClauseID = clauseID % LITS_PER_WORKGROUP;
      // initialize num_lit_undefs_local
      const int c_length = clause_lengths[clauseID];
      num_lit_undefs_local[localClauseID] = c_length;
   

      // decode assignment
      const int p = literals[literalID];
      const int psign = LIT_SIGN(p);
      
      const int offset = NUM_VARIABLES_ALIGNED * explorerID;
      const int var_offset = offset + LIT_VAR(p);
      const int remainder = (var_offset & 0x3) << 1;
      const uchar assignment_loc = assignments[var_offset >> 2];

      const uchar assignment = (assignment_loc >> remainder) & 0x3;

      // decode the three cases: true, false, undefined
      const int a_true = (assignment==psign);
      const int a_undef = (assignment==LBOOL_UNDEF);
      const int a_false = !a_undef && !a_true;

      if (a_true) {
        // this clause is SAT for this explorer
        // prevent other threads from concluding unit or conflict
        atomic_xchg(&num_lit_undefs_local[localClauseID], -1);
      } else if (a_false) {
        atomic_dec(&num_lit_undefs_local[localClauseID]);
      }
      
      const int undef = num_lit_undefs_local[localClauseID];
      //const int cid_ndx = (explorerID * MULTI_FACTOR) + clauseID % MULTI_FACTOR;
      if (undef == 1) {
        // unit
        // only set if nothing recorded in this slot
        //atomic_cmpxchg(&units_local[cid_local_ndx], 0, -clauseID - 1);
        //units_local[cid_local_ndx] = -clauseID - 1;
        
        // this clause might actually be conflict
        // if so, it will be written into conflicts by another thread
        // then, when conflicts and units are merged by the CPU later, 
        // the conflict will take priority and this bogus unit info
        // will be discarded
        // we will pay for this write whether it really happens or not
        //units[cid_ndx] = -clauseID - 1;
        unitClauseID = clauseID;

      } else if (undef == 0) {
        //atomic_xchg(&conflicts_local[cid_local_ndx], clauseID + 1);
        //conflicts_local[cid_local_ndx] = clauseID + 1;
        //conflicts[cid_ndx] = clauseID + 1;
        conflictClauseID = clauseID;
      }
    } // end if
  
  } // end for
 
  // upload if interesting
  if (conflictClauseID >= 0) {
    const int cid_ndx = (explorerID * MULTI_FACTOR) + conflictClauseID % MULTI_FACTOR;
    conflicts[cid_ndx] = conflictClauseID + 1;

  } else if (unitClauseID >= 0) {
    const int cid_ndx = (explorerID * MULTI_FACTOR) + unitClauseID % MULTI_FACTOR;
    units[cid_ndx] = -unitClauseID -1;
  } // end if

} // end proc
