// Macros for literals
#define LIT_SIGN(p) ((p) & 1)
#define LIT_VAR(p) ((p) >> 1)

// Lifted Boolean domain
#define LBOOL_TRUE 0
#define LBOOL_FALSE 1
#define LBOOL_UNDEF 2

// Statistics
#define STAT_BLOCK_SIZE 5
#define STAT_OFFSET_SAT 0
#define STAT_OFFSET_UNIT 1
#define STAT_OFFSET_CONFL 2
#define STAT_OFFSET_UNRES 3
#define STAT_OFFSET_WASTE 4


// Inspect one literal for one explorer
__kernel void per_literal_unified(
    // inputs
    const int num_explorers,
    const int num_variables,
    const int num_literals,
    const int num_clauses,
    __global const char* const assignments,     // size == num_explorers * num_variables
    __global const int*  const literals,        // size == sum of length of all clauses
    __global const int*  const lit2clause,      // size == same as literals[]
    // initialized to the number of literals in the clause
    __global int* const num_lit_undefs,     // size == num_explorers * num_clauses
    // MULTI_FACTOR is set by the pre-processor, not an argument
    // outputs
    // initialized to zeroes
    __global int* const cid,   // size == num_explorers * MULTI_FACTOR
    // initialized to zeroes
    __global int* const stats  // size == num_explorer * STAT_BLOCK_SIZE

    ) {

  // decode work item
  const int explorerID = get_global_id(0);
  const int literalID = get_global_id(1);

  // literalID might be greater than num_literals due to workgroup sizing
  if (literalID < num_literals) {
    // decode assignment
    const int offset = explorerID * ceil(num_variables / 4.0f);
    const int p = literals[literalID];
    const int psign = LIT_SIGN(p);
    const int var_offset = LIT_VAR(p) >> 2;
    const int remainder = (LIT_VAR(p) & 0x3) << 1;
    const uchar assignment_loc = assignments[offset + var_offset];
    // printf("%d, %d\n", offset + var_offset, (char)assignment_loc);
    const uchar assignment = (assignment_loc >> remainder) & 0x3;

    // decode the three cases: true, false, undefined
    const int a_true = (assignment==psign);
    const int a_undef = (assignment==LBOOL_UNDEF);
    const int a_false = !a_undef && !a_true;

    // some array indices we will need to record results of this literal inspection
    const int clauseID = lit2clause[literalID];
    const int explorerClauseID = (explorerID * num_clauses) + clauseID;
    const int cid_ndx = (explorerID * MULTI_FACTOR) + (clauseID % MULTI_FACTOR);

    if (a_true) {
      // this clause is SAT for this explorer
      // prevent other threads from concluding unit or conflict
      atomic_xchg(&num_lit_undefs[explorerClauseID], -1);
      // if the hash table has a record of this clause as unit, zero it out
      //atomic_cmpxchg(&cid[cid_ndx], -clauseID - 1, 0);
    } else if (a_false) {
      // decrement number of literals undefined
      // atomic_dec returns the old value
      // so to get the current value we subtract one
      const int undef = atomic_dec(&num_lit_undefs[explorerClauseID]) - 1;
      if (undef == 0) {
          // conflict
          // all literals for this clause have been inspected
          // none of them have been satisfied
          // this is the last thread to inspect a literal from this clause
          atomic_xchg(&cid[cid_ndx], clauseID + 1);
      }
    }
  } // end if


  // the barrier needs to be outside the conditional to prevent GPU crashes
  // this can become a barrier on local memory when num_lit_undefs is
  // in local memory (and clauses are aligned within workgroup
  // boundaries)
  // ok, this barrier is only good for the work-items in a workgroup
  // so if clauses are not aligned with workgroups this will not do
  // what we want
  barrier(CLK_GLOBAL_MEM_FENCE);

  // this condition might not hold due to workgroup sizing
  if (literalID < num_literals) {
    const int clauseID = lit2clause[literalID];
    const int explorerClauseID = (explorerID * num_clauses) + clauseID;
    const int undef = num_lit_undefs[explorerClauseID];
    if (undef == 1) {
      // unit
      // perhaps we can remove this atomic?
      const int cid_ndx = (explorerID * MULTI_FACTOR) + (clauseID % MULTI_FACTOR);
      atomic_cmpxchg(&cid[cid_ndx], 0, -clauseID - 1);
    }
  } // end if
} // end proc
