package ca.uwaterloo.miramichi.gpu;

import java.util.logging.Logger;

import ca.uwaterloo.miramichi.LogHelper;

public class DurationSummary {
  final String name;
  final Logger logger;
  final int width;
  long latencyDuration = 0;
  long computeDuration = 0;
  long queuedDuration = 0;

  public DurationSummary(final String name, final Logger logger, final int width) {
    this.name = name;
    this.logger = logger;
    this.width = width;
  }

  public void absorb(final DurationSummary d) {
    latencyDuration += d.latencyDuration;
    computeDuration += d.computeDuration;
    queuedDuration += d.queuedDuration;
  }

  /** Log the summary statistics. */
  public void log(final int cycles) {
    final LogHelper L = new LogHelper(logger, width);
    L.info(name + " queued", queuedDuration, cycles);
    L.info(name + " latency", latencyDuration, cycles);
    L.info(name + " compute", computeDuration, cycles);
  }

}
