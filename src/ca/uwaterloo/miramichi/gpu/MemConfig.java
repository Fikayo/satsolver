package ca.uwaterloo.miramichi.gpu;

import org.jocl.CL;

public class MemConfig {

  public static final MemConfig UploadOnly = new MemConfig(true, false, false, false,
      CL.CL_MEM_READ_ONLY);
  public static final MemConfig IncrementalUploadOnly = new MemConfig(true, true, true, false,
      CL.CL_MEM_READ_ONLY);
  public static final MemConfig IncrementalPersistentState = new MemConfig(true, true, true, false,
      CL.CL_MEM_READ_WRITE);
  public static final MemConfig TransientGPUState = new MemConfig(false, false, false, false,
      CL.CL_MEM_READ_WRITE);
  public static final MemConfig DownloadOnly = new MemConfig(false, false, false, true,
      CL.CL_MEM_WRITE_ONLY);
  public static final MemConfig UploadDownload = new MemConfig(true, false, false, true,
      CL.CL_MEM_READ_WRITE);

  public final boolean upload;
  public final boolean incremental_upload;
  public final boolean persistent;
  public final boolean download;

  /** Does the kernel read or write to this mem? */
  public final long device_access;


  public MemConfig(final boolean upload, final boolean incremental_upload,
      final boolean persistent, final boolean download, final long device_access) {
    super();
    this.upload = upload;
    this.incremental_upload = incremental_upload;
    this.persistent = persistent;
    this.download = download;
    this.device_access = device_access;
    assert configOk();
  }

  public boolean configOk() {
    if (incremental_upload) {
      assert upload : "must be uploadable to support incremental uploads";
      assert persistent : "must be persistent to support incremental uploads";
    }
    if (download) {
      assert device_access != CL.CL_MEM_READ_ONLY : "no need to download read-only memory (except for debugging)";
    }
    return true;
  }
}
