package ca.uwaterloo.miramichi.gpu;

import java.util.logging.Logger;

public class NopOpenCLComputation implements OpenCLComputation {

  @Override
  public Mem[] memories() {
    return new Mem[] {};
  }

  @Override
  public Kernel[] kernels() {
    return new Kernel[] {};
  }

  @Override
  public void uploadPreprocessing() {}

  @Override
  public void uploadPostprocessing() {}

  @Override
  public void downloadPreprocessing() {}

  @Override
  public void downloadPostprocessing() {}

  @Override
  public Logger logger() {
    return null;
  }

}
