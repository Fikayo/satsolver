package ca.uwaterloo.miramichi.gpu;

import java.util.List;
import java.util.logging.Logger;

import org.jocl.Sizeof;
import org.jocl.cl_event;

import ca.uwaterloo.miramichi.ClauseDB;


public class GPUEngine3 extends AbstractGPUEngine {

  /** All literals of all clauses. */
  private final MemInt literals = new MemInt("literals", MemConfig.IncrementalUploadOnly,
      new MemCapacity(-1, () -> MAX_NUM_LITERALS));

  /** Maps each literal index to the clause that literal belongs to. */
  private final MemInt lit2clause = new MemInt("lit2clause", MemConfig.IncrementalUploadOnly,
      new MemCapacity(-1, () -> MAX_NUM_LITERALS));

  /** Length of each clause. */
  private final MemInt clause_lengths = new MemInt("clause_lengths",
      MemConfig.IncrementalUploadOnly, new MemCapacity(-1, () -> MAX_NUM_CLAUSES));

  /** Count-down of number of undefined literals per clause per explorer. */
  private final MemInt num_lits_undef = new MemInt("num_lits_undef",
      MemConfig.IncrementalPersistentState, new MemCapacity(-1, () -> Math.min(500000000 / 4,
          MAX_NUM_CLAUSES * NUM_EXPLORERS)));

  /** Array of all GPUMems instantiated above, so they can be processed in bulk. */
  private final Mem[] memories = {assignments, literals, lit2clause, clause_lengths,
      num_lits_undef, cid, stats};

  public GPUEngine3(final ClauseDB DB, final int num_explorers) {
    super(DB, num_explorers);
    computer.initialize();
    assert DB.align : "ClauseDB must be aligned for this kernel";
  }

  @Override
  protected void consolidateToMems(int[] literals, int[] lit2clause, int[] offsets, int[] lengths) {
    this.literals.h = literals;
    this.lit2clause.h = lit2clause;
    this.clause_lengths.h = lengths;
    this.num_lits_undef.h = new int[offsets.length * NUM_EXPLORERS];
    assert num_literals_consolidated == this.literals.num_units_uploaded() + literals.length;
  }


  @Override
  public void uploadPreprocessing() {}

  @Override
  public void uploadPostprocessing() {
    assert literals.num_units_uploaded() == DB.numLiterals() : "literals uploaded: "
        + literals.num_units_uploaded() + " DB.numLiterals() " + DB.numLiterals();
  }

  @Override
  public void downloadPreprocessing() {}

  @Override
  public void downloadPostprocessing() {
    assert cid.h_length() == NUM_EXPLORERS * MULTI_FACTOR : "cid.h_length != NUM_EXPLORERS * MULTI_FACTOR"
        + cid.h_length() + " " + NUM_EXPLORERS + " " + MULTI_FACTOR;
  }

  @Override
  public Mem[] memories() {
    return memories;
  }


  @Override
  public Kernel[] kernels() {
    return kernels;
  }

  private final Kernel[] kernels = new Kernel[] {
      new Kernel("src/ca/uwaterloo/miramichi/gpu/init_num_lits_undef.cl", "init_num_lits_undef") {

        /** This global-copy kernel should eventually be replaced with a copy to local memory. */
        @Override
        public cl_event prepareLaunchInner() {
          // inputs
          setArg("num_clauses", Sizeof.cl_int, OpenCLUtil.pointerToInt(num_clauses_consolidated));
          setArg("num_literals", Sizeof.cl_int, OpenCLUtil.pointerToInt(num_literals_consolidated));
          setArg("clause_lengths", Sizeof.cl_mem, clause_lengths.d_pointer());
          // output
          setArg("num_lits_undef", Sizeof.cl_mem, num_lits_undef.d_pointer());

          // set kernel launch parameters
          final int work_x = NUM_EXPLORERS;
          final int work_y = (int) fitToWorkgroup(num_clauses_consolidated);
          total_work_items += work_x * work_y;

          work_dim = 2;
          global_work_offset = null;
          global_work_size = new long[] {work_x, work_y};
          local_work_size = new long[] {1, maxWorkgroupSize};
          num_events_in_wait_list = 0;
          event_wait_list = null;
          event = AbstractGPUEngine.GPU_PROFILING ? new cl_event() : null;

          return event;
        }
      },
      new Kernel("src/ca/uwaterloo/miramichi/gpu/per_literal_unified.cl", "per_literal_unified") {

        @Override
        protected List<String> preprocessorDirectives() {
          final List<String> p = super.preprocessorDirectives();
          p.add("#define MULTI_FACTOR " + MULTI_FACTOR);
          return p;
        }

        @Override
        public cl_event prepareLaunchInner() {
          // sanity checks
          assert literals.num_units_uploaded() > 0;
          assert assignments.num_units_uploaded() > 0;
          assert lit2clause.num_units_uploaded() > 0;

          // inputs
          setArg("num_explorers", Sizeof.cl_int, OpenCLUtil.pointerToInt(NUM_EXPLORERS));
          setArg("num_variables", Sizeof.cl_int, OpenCLUtil.pointerToInt(DB.varCount()));
          setArg("num_literals", Sizeof.cl_int,
              OpenCLUtil.pointerToInt(literals.num_units_uploaded()));
          setArg("num_clauses", Sizeof.cl_int, OpenCLUtil.pointerToInt(num_clauses_consolidated));
          setArg("assignments", Sizeof.cl_mem, assignments.d_pointer());
          setArg("literals", Sizeof.cl_mem, literals.d_pointer());
          setArg("lit2clause", Sizeof.cl_mem, lit2clause.d_pointer());
          setArg("num_lits_undef", Sizeof.cl_mem, num_lits_undef.d_pointer());
          // outputs
          setArg("d_cid", Sizeof.cl_mem, cid.d_pointer());
          setArg("d_stats", Sizeof.cl_mem, stats.d_pointer());

          // set kernel launch parameters
          final int work_x = NUM_EXPLORERS;
          final int work_y = (int) fitToWorkgroup(literals.num_units_uploaded());
          final int work_items = work_x * work_y;
          total_work_items += work_items;

          work_dim = 2;
          global_work_offset = null;
          global_work_size = new long[] {work_x, work_y};
          local_work_size = new long[] {1, maxWorkgroupSize};
          num_events_in_wait_list = 0;
          event_wait_list = null;
          event = AbstractGPUEngine.GPU_PROFILING ? new cl_event() : null;

          return event;
        }
      }};

  private static final Logger logger = Logger.getLogger(GPUEngine3.class.getSimpleName());

  /** For access from super-class. */
  @Override
  public Logger logger() {
    return logger;
  }

}
