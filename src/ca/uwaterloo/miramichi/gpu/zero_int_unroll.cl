// step = workgroup size
// ub/step = size of unroll
// ub = unroll * step
// size of array must be a multiple of upperbound
// number of work items must be a multiple of upperbound
// size of array / unroll = number of work items
// upperbound is the number of indices cleared by one workgroup

// zero out an array of integers
__kernel void zero_int_unroll(__global int* const a, const int length)
{
    const int tid = get_global_id(0);
    #pragma unroll UNROLL
    for (int i =tid; i < UPPERBOUND; i += STEP) {
        a[i] = 0;
    }
}
