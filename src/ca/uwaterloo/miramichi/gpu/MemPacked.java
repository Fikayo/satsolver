package ca.uwaterloo.miramichi.gpu;

import org.jocl.Pointer;
import org.jocl.Sizeof;
import org.jocl.cl_kernel;

import ca.uwaterloo.miramichi.PackedArray;

public final class MemPacked extends Mem {

  public static MemPacked EMPTY_MEM_PACKED() {
    return new MemPacked("EMPTY_MEM", MemConfig.IncrementalPersistentState,
        new MemCapacity(() -> 1), 32);
  }


  static cl_kernel zero_kernel = null;

  private PackedArray h;
  private final int bitwidth;
  private final int valuesPerCell;
  private PackedArray leftover;
  private int numValuesUploadedTotal;
  private int numValuesUploadedTwice;
  private int numValuesUploadedTwicePending;


  public MemPacked(final String name, final MemConfig config, final MemCapacity capacity,
      final int requestedBitwidth) {
    super(name, config, capacity);
    final PackedArray a = new PackedArray(requestedBitwidth, 1);
    this.bitwidth = a.bitwidth;
    this.valuesPerCell = a.valuesPerCell;
    this.leftover = new PackedArray(requestedBitwidth, 0);
  }

  @Override
  public int deviceSizeUnit() {
    return Sizeof.cl_int;
  }

  @Override
  public void initHost() {
    // initialize mem on CPU
    final int h_capacity = capacity.hostCapacity() * valuesPerCell;
    if (h_capacity <= 0) {
      // this will be set dynamically by the client
      h = null;
    } else {
      // this is a static buffer that we initialize here
      initHost(h_capacity);
    }
  }

  public void initHost(final int logicalCapacity) {
    // initialize mem on CPU
    assert capacity.hostCapacity() <= 0 : "capacity is pre-determined, cannot be set dynamically";

    // System.out.println("initHost: " + leftover.size());
    h = new PackedArray(bitwidth, logicalCapacity + leftover.size());
    for (int i = 0; i < leftover.size(); i++) {
      final int x = leftover.get(i);
      // System.out.println("packing leftover: " + x);
      h.set(i, x);
      numValuesUploadedTwicePending++;
    }
    // leftover is now stale
    // we keep it around to compute d_offset()
  }

  /** The number of logical values uploaded. */
  public int num_values_uploaded() {
    return numValuesUploadedTotal - numValuesUploadedTwice;
  }

  public void h_append(final int value) {
    h.append(value);
  }

  @Override
  public int h_length() {
    return (null == h) ? 0 : h.getCells().length;
  }

  @Override
  public boolean h_isNull() {
    return null == h;
  }

  @Override
  public void h_clear() {
    if (null != h) {
      // leftover should not be cleared here
      initHost(h.capacity());
    }
  }

  @Override
  public Pointer h_pointer() {
    return Pointer.to(h.getCells());
  };

  @Override
  protected int d_offset() {
    if (config.incremental_upload) {
      return PackedArray.cellCount(num_values_uploaded(), valuesPerCell)
          - (leftover.size() > 0 ? 1 : 0);
    } else {
      return 0;
    }
  }

  @Override
  public void releaseHost() {
    // this is called just after upload and at the end to release resources
    // TODO: separate these two distinct uses of this method
    if (null != h) {
      numValuesUploadedTotal += h.size();
      numValuesUploadedTwice += numValuesUploadedTwicePending;
      numValuesUploadedTwicePending = 0;
      leftover = h.getLeftovers();
      // System.out.println("releaseHost: " + Util.i2s(h.size(), leftover.size()));
      h = null;
    }
  }

  @Override
  public cl_kernel d_zero_kernel() {
    return zero_kernel;
  }

}
