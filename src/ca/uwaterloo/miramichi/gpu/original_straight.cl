/**
 * @file    opencl_query.cl
 * @author  Steven.Stewart@uwaterloo.ca
 */

// Macros for literals
#define LIT_SIGN(p) ((p) & 1)
#define LIT_VAR(p) ((p) >> 1)

// Lifted Boolean domain
#define LBOOL_TRUE 0
#define LBOOL_FALSE 1
#define LBOOL_UNDEF 2

// Statistics
#define STAT_BLOCK_SIZE 5
#define STAT_OFFSET_SAT 0
#define STAT_OFFSET_UNIT 1
#define STAT_OFFSET_CONFL 2
#define STAT_OFFSET_UNRES 3
#define STAT_OFFSET_WASTE 4


/**
 * This kernel performs a step of BCP, called a query, using an OpenCL device. This step is executed for
 * multiple explorers, whose state (partial assignments) are stored in the assignments array. The result
 * of the query is either a clause id (unit or conflicting), or tha a fixed point has been reached. Some
 * statistical counts are gathered, which may also be part of the query result, when copied to the host.
 *
 * @param[in]   num_variables   Length of the variable assignment array.
 * @param[in]   assignments     State of all explorers (their assignments).
 * @param[in]   num_explorers   Number of explorers involved in the query.
 * @param[in]   num_literals    Number of literals in the clause database.
 * @param[in]   literals        Device memory address of the literals.
 * @param[in]   num_clauses     Number of clauses in the clause database.
 * @param[in]   clause_index    Index for looking up offset of a clause in literals.
 * @param[out]  cid             For each explorer, an encoded clause id of a unit or conflicting clause.
 *                              Result type indicated as follows: <0 = unit, >0 = conflict, 0 = done.
 * @param[out]  stats           For each explorer, statistics for number of unit, confl, sat, and "waste" clauses
 */
__kernel void opencl_query(
                           const int num_variables,
                           __global const char* const assignments,
                           const int num_explorers,
                           const int num_literals,
                           __global const int* const literals,
                           const int num_clauses,
                           __global const int* const clause_index,
                           __global int* const cid,
                           __global int* const stats
                           )
{
    // Obtain thread id
    // tid encodes the explorerID and clauseID that we are going to work on
    // low bits are explorerID
    // high bits are clauseID
    const int tid = get_global_id(0);
    const int explorerID = tid / num_clauses;
    const int clauseID = tid - (explorerID * num_clauses);
    
    // stop computing if we're out of bounds
    if (explorerID > num_explorers) { return; }
    if (clauseID > num_clauses-1) { return; }
    
    // set "begin" to be the offset of the first literal of this thread's clause
    const size_t begin = clause_index[clauseID];
    
    // set "end" to be the offset of the last literal of this thread's clause
    const size_t end = (clauseID == num_clauses - 1) ? (num_literals - 1) : (clause_index[clauseID + 1] - 1);
    
    const int offset = explorerID * num_variables;

// no longer looping over explorers    
//    for (int i = 0, offset = 0; i < num_explorers; ++i, offset += num_variables) {
        int sat_flag = 0;           // flag for skipping "process result" step
        int num_lit_undef = 0;      // number of unassigned literals in the clause
        int non_waste_count = 0;
        
        // (1) inspect: check each literal of the clause
        for (int j = begin; j <= end; ++j) {
            const int p = literals[j];
            const int psign = LIT_SIGN(p);
            const char assignment = assignments[LIT_VAR(p) + offset];

            // decode the three cases: true, false, undefined
            const int a_true = (assignment==psign);
            const int a_undef = (assignment==LBOOL_UNDEF);
            
            // set sat_flag if literal is true
            sat_flag = a_true;

            // count number of undefined literals
            num_lit_undef += a_undef;

            // anything defined is not wasted
            non_waste_count += !a_undef;
            
            // break out if clause is satisfied
            if (sat_flag) { break; }
        }

        // inspection results
        const int waste_flag = (non_waste_count == 0);
        const int unit_flag = !sat_flag && !waste_flag && (num_lit_undef == 1);
        const int confl_flag = !sat_flag && (num_lit_undef == 0);
        const int unres_flag = !sat_flag && !waste_flag && (num_lit_undef > 1);

		const int cid_ndx = (explorerID * MULTI_FACTOR) + (clauseID % MULTI_FACTOR);
// This pre-processor flag is defined when GPUEngine loads this file into a string.                
#ifdef GPU_PROFILING
        // check flags are mutually exclusive
        const int sum = sat_flag + waste_flag + unit_flag + confl_flag + unres_flag;
        if (sum > 1) return;
         
        // decode result of inspection for stat recording
        // at most one of these flags is true
        const int stat_offset =
            sat_flag * STAT_OFFSET_SAT +
            waste_flag * STAT_OFFSET_WASTE +
            unit_flag * STAT_OFFSET_UNIT +
            confl_flag * STAT_OFFSET_CONFL +
            unres_flag * STAT_OFFSET_CONFL
            ;
      
        // increment appropriate stat counter  
        atomic_inc(&stats[explorerID * STAT_BLOCK_SIZE + stat_offset]);
#endif
        // record clause id in interesting cases: unit and conflict        
        if (unit_flag) {
            atomic_cmpxchg(&cid[cid_ndx], 0, -clauseID - 1);
        }
        else if (confl_flag) {
            cid[cid_ndx] = clauseID + 1;
        }
//    }
}
