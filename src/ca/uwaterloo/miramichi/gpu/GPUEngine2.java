package ca.uwaterloo.miramichi.gpu;

import java.util.List;
import java.util.logging.Logger;

import org.jocl.Sizeof;
import org.jocl.cl_event;

import ca.uwaterloo.miramichi.ClauseDB;


public class GPUEngine2 extends AbstractGPUEngine {

  /** All literals of all clauses. */
  private final MemInt literals = new MemInt("literals", MemConfig.IncrementalUploadOnly,
      new MemCapacity(-1, () -> MAX_NUM_LITERALS));

  /** Maps each literal index to the clause that literal belongs to. */
  private final MemInt lit2clause = new MemInt("lit2clause", MemConfig.IncrementalUploadOnly,
      new MemCapacity(-1, () -> MAX_NUM_LITERALS));

  /** Records the number of satisfied literals for each clause for each explorer. */
  private final MemByte sat_flags = new MemByte("sat_flags", MemConfig.IncrementalPersistentState,
      new MemCapacity(-1, () -> Math.min(500000000, MAX_NUM_CLAUSES * NUM_EXPLORERS)));

  /** Count number of undefined literals per clause per explorer. */
  private final MemInt num_lit_undefs = new MemInt("num_lit_undefs",
      MemConfig.IncrementalPersistentState, new MemCapacity(-1, () -> Math.min(500000000 / 4,
          MAX_NUM_CLAUSES * NUM_EXPLORERS)));

  /** Count number of non-waste literals per clause per explorer. */
  private final MemByte non_waste_flags = new MemByte("non_waste_flags",
      MemConfig.IncrementalPersistentState, new MemCapacity(-1, () -> Math.min(500000000,
          MAX_NUM_CLAUSES * NUM_EXPLORERS)));

  /** Array of all GPUMems instantiated above, so they can be processed in bulk. */
  private final Mem[] memories = {assignments, literals, lit2clause, sat_flags, num_lit_undefs,
      non_waste_flags, cid, stats};

  public GPUEngine2(final ClauseDB DB, final int num_explorers) {
    super(DB, num_explorers);
    computer.initialize();
  }

  @Override
  protected void consolidateToMems(int[] literals, int[] lit2clause, int[] offsets, int[] lengths) {
    this.literals.h = literals;
    this.lit2clause.h = lit2clause;
    this.sat_flags.h = new byte[offsets.length * NUM_EXPLORERS];
    this.num_lit_undefs.h = new int[offsets.length * NUM_EXPLORERS];
    this.non_waste_flags.h = new byte[offsets.length * NUM_EXPLORERS];

    assert num_literals_consolidated == this.literals.num_units_uploaded() + literals.length;
  }

  @Override
  public void uploadPreprocessing() {}

  @Override
  public void uploadPostprocessing() {
    assert literals.num_units_uploaded() == DB.numLiterals() : "literals uploaded: "
        + literals.num_units_uploaded() + " DB.numLiterals() " + DB.numLiterals();
  }

  @Override
  public void downloadPreprocessing() {}

  @Override
  public void downloadPostprocessing() {
    assert cid.h_length() == NUM_EXPLORERS * MULTI_FACTOR : "cid.h_length != NUM_EXPLORERS * MULTI_FACTOR"
        + cid.h_length() + " " + NUM_EXPLORERS + " " + MULTI_FACTOR;
  }

  @Override
  public Mem[] memories() {
    return memories;
  }


  @Override
  public Kernel[] kernels() {
    return kernels;
  }

  private final Kernel[] kernels = new Kernel[] {
      new Kernel("src/ca/uwaterloo/miramichi/gpu/per_literal.cl", "per_literal") {

        @Override
        public cl_event prepareLaunchInner() {
          // sanity checks
          assert literals.num_units_uploaded() > 0;
          assert assignments.num_units_uploaded() > 0;
          assert lit2clause.num_units_uploaded() > 0;

          // inputs
          setArg("num_explorers", Sizeof.cl_int, OpenCLUtil.pointerToInt(NUM_EXPLORERS));
          setArg("num_variables", Sizeof.cl_int, OpenCLUtil.pointerToInt(DB.varCount()));
          setArg("num_literals", Sizeof.cl_int,
              OpenCLUtil.pointerToInt(literals.num_units_uploaded()));
          setArg("num_clauses", Sizeof.cl_int, OpenCLUtil.pointerToInt(num_clauses_consolidated));
          setArg("assignments", Sizeof.cl_mem, assignments.d_pointer());
          setArg("literals", Sizeof.cl_mem, literals.d_pointer());
          setArg("lit2clause", Sizeof.cl_mem, lit2clause.d_pointer());
          // outputs
          setArg("sat_flags", Sizeof.cl_mem, sat_flags.d_pointer());
          setArg("num_lit_undefs", Sizeof.cl_mem, num_lit_undefs.d_pointer());
          setArg("non_waste_flags", Sizeof.cl_mem, non_waste_flags.d_pointer());

          // set kernel launch parameters
          final int work_items = NUM_EXPLORERS * literals.num_units_uploaded();
          total_work_items += work_items;

          work_dim = 1;
          global_work_offset = null;
          global_work_size = new long[] {work_items};
          local_work_size = null;
          num_events_in_wait_list = 0;
          event_wait_list = null;
          event = AbstractGPUEngine.GPU_PROFILING ? new cl_event() : null;

          return event;
        }
      }, new Kernel("src/ca/uwaterloo/miramichi/gpu/per_clause.cl", "per_clause") {

        @Override
        protected List<String> preprocessorDirectives() {
          final List<String> p = super.preprocessorDirectives();
          p.add("#define MULTI_FACTOR " + MULTI_FACTOR);
          return p;
        }

        @Override
        public cl_event prepareLaunchInner() {
          // Set arguments for kernel
          // inputs
          setArg("num_explorers", Sizeof.cl_int, OpenCLUtil.pointerToInt(NUM_EXPLORERS));
          setArg("num_variables", Sizeof.cl_int, OpenCLUtil.pointerToInt(DB.varCount()));
          setArg("num_literals", Sizeof.cl_int,
              OpenCLUtil.pointerToInt(literals.num_units_uploaded()));
          setArg("num_clauses", Sizeof.cl_int, OpenCLUtil.pointerToInt(num_clauses_consolidated));
          // inputs from per_literal kernel
          setArg("sat_flags", Sizeof.cl_mem, sat_flags.d_pointer());
          setArg("num_lit_undefs", Sizeof.cl_mem, num_lit_undefs.d_pointer());
          setArg("non_waste_flags", Sizeof.cl_mem, non_waste_flags.d_pointer());
          // outputs
          setArg("d_cid", Sizeof.cl_mem, cid.d_pointer());
          setArg("d_stats", Sizeof.cl_mem, stats.d_pointer());

          // set kernel launch parameters
          final int work_items = NUM_EXPLORERS * num_clauses_consolidated;
          total_work_items += work_items;

          work_dim = 1;
          global_work_offset = null;
          global_work_size = new long[] {work_items};
          local_work_size = null;
          num_events_in_wait_list = 0;
          event_wait_list = null;
          event = AbstractGPUEngine.GPU_PROFILING ? new cl_event() : null;

          return event;
        }
      }};

  private static final Logger logger = Logger.getLogger(GPUEngine2.class.getSimpleName());

  /** For access from super-class. */
  @Override
  public Logger logger() {
    return logger;
  }

}
