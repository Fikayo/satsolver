// initialize the num_lit_undef array for each explorer
__kernel void init_num_lits_undef(
    const int num_clauses,
    const int num_literals,
    __global const int* const clause_lengths, 
    __global int* const num_lits_undef
    )
{
    const int explorerID = get_global_id(0);
    const int clauseID = get_global_id(1);

    // this condition might not hold due to workgroup sizing
    if (clauseID < num_clauses) {
      const int ndx = explorerID * num_clauses + clauseID;
      // this condition should always hold, but check anyways
      if (ndx < num_literals) {
        num_lits_undef[ndx] = clause_lengths[clauseID];
      }
    }
}
