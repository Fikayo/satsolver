package ca.uwaterloo.miramichi.gpu;

import java.util.List;
import java.util.logging.Logger;

import org.jocl.Sizeof;
import org.jocl.cl_event;

import ca.uwaterloo.miramichi.Assignment;
import ca.uwaterloo.miramichi.ClauseDB;


public class GPUEngine7 extends AbstractGPUEngine {

  private final static int UNROLL = 16;

  /** All literals of all clauses, as well as the clause they belong to. */
  private final MemPacked literals = new MemPacked("literals", MemConfig.IncrementalUploadOnly,
      new MemCapacity(-1, () -> MAX_NUM_LITERALS), 16);

  /** Length of each clause. */
  private final MemInt clause_lengths = new MemInt("clause_lengths",
      MemConfig.IncrementalUploadOnly, new MemCapacity(-1, () -> MAX_NUM_CLAUSES));

  /**
   * Holds an encoded clause id (id of unit or conflicting clause) for each explorer. Also encoded
   * in this value is the result type (<0 = unit, >0 = conflict, 0 = done)
   */
  private final MemInt units = new MemInt("units", MemConfig.DownloadOnly, new MemCapacity(
      () -> numExplorers() * AbstractGPUEngine.MULTI_FACTOR));
  private final MemInt conflicts = new MemInt("conflicts", MemConfig.DownloadOnly, new MemCapacity(
      () -> numExplorers() * AbstractGPUEngine.MULTI_FACTOR));


  /** Array of all GPUMems instantiated above, so they can be processed in bulk. */
  private final Mem[] memories = {assignments, literals, clause_lengths, units, conflicts, stats};

  public GPUEngine7(final ClauseDB DB, final int num_explorers) {
    super(DB, num_explorers);
    computer.initialize();
    assert DB.align : "ClauseDB must be aligned for this kernel";
  }

  @Override
  protected void consolidateToMems(int[] literals, int[] lit2clause, int[] offsets, int[] lengths) {
    // store literals and lit2clause in the same packed array
    this.literals.initHost(literals.length * 2);
    for (int i = 0; i < literals.length; i++) {
      this.literals.h_append(literals[i]);
      this.literals.h_append(lit2clause[i]);
    }
    this.clause_lengths.h = lengths;
    assert num_literals_consolidated == this.literals.num_units_uploaded() + literals.length;
  }

  @Override
  protected boolean shouldCheckTotalStats() {
    return false;
  }

  @Override
  public void uploadPreprocessing() {}

  @Override
  public void uploadPostprocessing() {
    assert literals.num_units_uploaded() == DB.numLiterals() : "literals uploaded: "
        + literals.num_units_uploaded() + " DB.numLiterals() " + DB.numLiterals();
  }

  @Override
  public void downloadPreprocessing() {}

  @Override
  public void downloadPostprocessing() {
    // assert cid.h_length() == NUM_EXPLORERS * MULTI_FACTOR :
    // "cid.h_length != NUM_EXPLORERS * MULTI_FACTOR"
    // + cid.h_length() + " " + NUM_EXPLORERS + " " + MULTI_FACTOR;

    // merge units and conflicts into cid
    // TODO: move this somewhere else
    cid.h = new int[NUM_EXPLORERS * MULTI_FACTOR];
    for (int i = 0; i < cid.h_length(); i++) {
      final int u = units.h[i];
      final int c = conflicts.h[i];
      if (c > 0) {
        cid.h[i] = c;
      } else if (u < 0) {
        cid.h[i] = u;
      }
    }
  }

  @Override
  public Mem[] memories() {
    return memories;
  }


  @Override
  public Kernel[] kernels() {
    return kernels;
  }

  private final Kernel[] kernels = new Kernel[] {new MiramichiKernel(
      "src/ca/uwaterloo/miramichi/gpu/gpu7.cl", "gpu7_loop", DB) {

    @Override
    protected List<String> preprocessorDirectives() {
      final List<String> p = super.preprocessorDirectives();
      p.add("#define MULTI_FACTOR " + MULTI_FACTOR);
      p.add("#define UNROLL " + UNROLL);
      p.add("#define LITS_PER_WORKGROUP " + (wavefrontSize * UNROLL));
      final int cells = Assignment.cellCount(DB.varCount());
      final int floor = (int) Math.floor((double) cells / (double) this.wavefrontSize);
      final int ceil = (int) Math.ceil((double) cells / (double) this.wavefrontSize);
      p.add("#define ASSIGN_LOOP_BOUND_FLOOR " + floor);
      p.add("#define ASSIGN_LOOP_BOUND_CEIL " + ceil);

      return p;
    }

    @Override
    public cl_event prepareLaunchInner() {
      // sanity checks
      assert literals.num_units_uploaded() > 0;
      assert assignments.num_units_uploaded() > 0;

      // inputs
      setArg("num_explorers", Sizeof.cl_int, OpenCLUtil.pointerToInt(NUM_EXPLORERS));
      setArg("num_variables", Sizeof.cl_int, OpenCLUtil.pointerToInt(DB.varCount()));
      setArg("num_literals", Sizeof.cl_int, OpenCLUtil.pointerToInt(literals.num_units_uploaded()));
      setArg("num_clauses", Sizeof.cl_int, OpenCLUtil.pointerToInt(num_clauses_consolidated));
      setArg("assignments", Sizeof.cl_mem, assignments.d_pointer());
      setArg("literals", Sizeof.cl_mem, literals.d_pointer());
      setArg("clause_lengths", Sizeof.cl_mem, clause_lengths.d_pointer());
      // outputs
      // setArg("d_cid", Sizeof.cl_mem, cid.d_pointer());
      setArg("d_units", Sizeof.cl_mem, units.d_pointer());
      setArg("d_conflicts", Sizeof.cl_mem, conflicts.d_pointer());
      setArg("d_stats", Sizeof.cl_mem, stats.d_pointer());

      // set kernel launch parameters
      final int work_x = NUM_EXPLORERS;
      final int litsPerThread =
          (int) Math.ceil((double) literals.num_units_uploaded() / (double) UNROLL);
      final int work_y = (int) fitToWorkgroup(litsPerThread);
      final int work_items = work_x * work_y;
      total_work_items += work_items;

      work_dim = 2;
      global_work_offset = null;
      global_work_size = new long[] {work_x, work_y};
      local_work_size = new long[] {1, wavefrontSize};
      num_events_in_wait_list = 0;
      event_wait_list = null;
      event = AbstractGPUEngine.GPU_PROFILING ? new cl_event() : null;

      return event;
    }
  }};

  private static final Logger logger = Logger.getLogger(GPUEngine7.class.getSimpleName());

  /** For access from super-class. */
  @Override
  public Logger logger() {
    return logger;
  }

}
