package ca.uwaterloo.miramichi.gpu;

import java.util.logging.Logger;

import ca.uwaterloo.miramichi.ClauseDB;

public class GPUEngine1straight extends GPUEngine1 {

  public GPUEngine1straight(final ClauseDB DB, final int num_explorers) {
    super(DB, num_explorers);
  }

  @Override
  protected String kernelFile() {
    return "src/ca/uwaterloo/miramichi/gpu/original_straight.cl";
  }

  private static final Logger logger = Logger.getLogger(GPUEngine1straight.class.getSimpleName());

  /** For access from super-class. */
  @Override
  public Logger logger() {
    return logger;
  }

}
