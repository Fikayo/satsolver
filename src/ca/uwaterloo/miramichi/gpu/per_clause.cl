// Macros for literals
#define LIT_SIGN(p) ((p) & 1)
#define LIT_VAR(p) ((p) >> 1)

// Lifted Boolean domain
#define LBOOL_TRUE 0
#define LBOOL_FALSE 1
#define LBOOL_UNDEF 2

// Statistics
#define STAT_BLOCK_SIZE 5
#define STAT_OFFSET_SAT 0
#define STAT_OFFSET_UNIT 1
#define STAT_OFFSET_CONFL 2
#define STAT_OFFSET_UNRES 3
#define STAT_OFFSET_WASTE 4


// Summarize literal inspections for each clause for each explorer
__kernel void per_clause(
    // inputs
    const int num_explorers,
    const int num_variables,
    const int num_literals,
    const int num_clauses,
    __global char* const sat_flags,    // size == num_explorers * num_clauses
    __global int* const num_lit_undefs, // size == num_explorers * num_clauses
    __global char* const non_waste_flags, // size == num_explorers * num_clauses
    // outputs
    __global int* const cid,   // size == num_explorers * MULTI_FACTOR
    __global int* const stats  // size == num_explorer * STAT_BLOCK_SIZE
    ) {

  // obtain thread id and decode work unit
  const int tid = get_global_id(0);
  const int explorerID = tid / num_clauses;
  const int clauseID = tid - (explorerID * num_clauses);
  const int explorerClauseID = tid;

  // read out results of literal inspection
  const int sat_flag = sat_flags[explorerClauseID];
  int confl_flag = num_lit_undefs[explorerClauseID] == 0 && !sat_flag;
  int unit_flag = num_lit_undefs[explorerClauseID] == 1 && !sat_flag;
  const int waste_flag = ((non_waste_flags[explorerClauseID] == 1) ? 0 : 1) && !unit_flag;
  const int unres_flag = (sat_flag || confl_flag || unit_flag || waste_flag) ? 0 : 1;
  
  const int cid_ndx = (explorerID * MULTI_FACTOR) + (clauseID % MULTI_FACTOR);

  // process result: update output parameters
#ifdef ASSERTIONS
  // record stats when assertions are enabled
  if (sat_flag) {
    atomic_inc(&stats[explorerID * STAT_BLOCK_SIZE + STAT_OFFSET_SAT]);
  }
  if (waste_flag) {
    atomic_inc(&stats[explorerID * STAT_BLOCK_SIZE + STAT_OFFSET_WASTE]);
  }
  if (unit_flag) {
    atomic_inc(&stats[explorerID * STAT_BLOCK_SIZE + STAT_OFFSET_UNIT]);
    atomic_cmpxchg(&cid[cid_ndx], 0, -clauseID - 1);
  }
  if (confl_flag) {
    atomic_inc(&stats[explorerID * STAT_BLOCK_SIZE + STAT_OFFSET_CONFL]);
    cid[cid_ndx] = clauseID + 1;
  }
  if (unres_flag) {
    atomic_inc(&stats[explorerID * STAT_BLOCK_SIZE + STAT_OFFSET_UNRES]);
  }
#else
  // assertions disabled, do not record stats
  if (unit_flag) {
    // unit
    atomic_cmpxchg(&cid[cid_ndx], 0, -clauseID - 1);
  }
  if (confl_flag) {
    // conflict
    cid[cid_ndx] = clauseID + 1;
  }
#endif
  sat_flags[explorerClauseID] = 0;
  num_lit_undefs[explorerClauseID] = 0;
  non_waste_flags[explorerClauseID] = 0;
}
