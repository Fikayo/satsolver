package ca.uwaterloo.miramichi.gpu;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ca.uwaterloo.miramichi.Assignment;
import ca.uwaterloo.miramichi.ClauseDB;

public abstract class MiramichiKernel extends Kernel {

  private final List<String> preprocessorDirectives;

  public MiramichiKernel(final String fileName, final String functionName, final ClauseDB DB) {
    super(fileName, functionName);
    final List<String> p = new ArrayList<>();
    p.add("#define NUM_VARIABLES " + DB.varCount());
    p.add("#define NUM_VARIABLES_ALIGNED "
        + (Assignment.cellCount(DB.varCount()) * Assignment.ASSIGNMENTS_PER_CELL));
    p.add("#define ASSIGNMENT_CELLS " + Assignment.cellCount(DB.varCount()));
    this.preprocessorDirectives = Collections.unmodifiableList(p);
  }

  /**
   * May be overridden by subclasses. Subclasses should start their implementation by calling this
   * method.
   */
  @Override
  protected List<String> preprocessorDirectives() {
    final List<String> p = super.preprocessorDirectives();
    p.addAll(preprocessorDirectives);
    return p;
  }

}
