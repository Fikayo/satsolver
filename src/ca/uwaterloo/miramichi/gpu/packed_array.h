
// 16 bit values
#define bitwidth           16
#define bitwidthLog2        4
#define valuesPerCell       2
#define valuesPerCellLog2   1
#define valueMask       65535
#define indexMask           1

/*
// 32 bits
#define bitwidth           32
#define bitwidthLog2        5
#define valuesPerCell       1
#define valuesPerCellLog2   0
#define valueMask           0xFFFFFFFF
#define indexMask           0
*/

int cell(const int index) {
  // these expressions are equivalent when bitwidth is a power of 2
  // return index / valuesPerCell;
  return index >> valuesPerCellLog2;
}

int subcell(const int index) {
  return (index & indexMask) << bitwidthLog2;
}

int get2(__global const uint* const a, const int cell, const int subcell) {
  return (a[cell] >> subcell) & valueMask;
}

int get(__global const uint* const a, const int index) {
  return get2(a, cell(index), subcell(index));
}


