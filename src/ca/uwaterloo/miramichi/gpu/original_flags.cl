/**
 * @file    opencl_query.cl
 * @author  Steven.Stewart@uwaterloo.ca
 */

// Macros for literals
#define LIT_SIGN(p) ((p) & 1)
#define LIT_VAR(p) ((p) >> 1)

// Lifted Boolean domain
#define LBOOL_TRUE 0
#define LBOOL_FALSE 1
#define LBOOL_UNDEF 2

// Statistics
#define STAT_BLOCK_SIZE 5
#define STAT_OFFSET_SAT 0
#define STAT_OFFSET_UNIT 1
#define STAT_OFFSET_CONFL 2
#define STAT_OFFSET_UNRES 3
#define STAT_OFFSET_WASTE 4

/**
 * This kernel performs a step of BCP, called a query, using an OpenCL device. This step is executed for
 * multiple explorers, whose state (partial assignments) are stored in the assignments array. The result
 * of the query is either a clause id (unit or conflicting), or tha a fixed point has been reached. Some
 * statistical counts are gathered, which may also be part of the query result, when copied to the host.
 *
 * @param[in]   num_variables   Length of the variable assignment array.
 * @param[in]   assignments     State of all explorers (their assignments).
 * @param[in]   num_explorers   Number of explorers involved in the query.
 * @param[in]   num_literals    Number of literals in the clause database.
 * @param[in]   literals        Device memory address of the literals.
 * @param[in]   num_clauses     Number of clauses in the clause database.
 * @param[in]   clause_index    Index for looking up offset of a clause in literals.
 * @param[out]  cid             For each explorer, an encoded clause id of a unit or conflicting clause.
 *                              Result type indicated as follows: <0 = unit, >0 = conflict, 0 = done.
 * @param[out]  stats           For each explorer, statistics for number of unit, confl, sat, and "waste" clauses
 */
__kernel void opencl_query(
                           const int num_variables,
                           __global const char* const assignments,
                           const int num_explorers,
                           const int num_literals,
                           __global const int* const literals,
                           const int num_clauses,
                           __global const int* const clause_index,
                           __global int* const cid,
                           __global int* const stats
                           )
{
    // Obtain thread id
    const int tid = get_global_id(0);
    if (tid >= num_clauses) {
        return;
    }
    
    // set "begin" to be the offset of the first literal of this thread's clause
    const size_t begin = clause_index[tid];
    
    // set "end" to be the offset of the last literal of this thread's clause
    const size_t end = (tid == num_clauses - 1) ? (num_literals - 1) : (clause_index[tid + 1] - 1);
    
    // for each explorer, (1) inspect the clause, then (2) process
    // the result of that inspection
    for (int i = 0, offset = 0; i < num_explorers; ++i, offset += num_variables) {
        int sat_flag = 0;           // flag for skipping "process result" step
        int waste_flag = 1;         // flag indicating a wasted inspection
        int num_lit_undef = 0;      // number of unassigned literals in the clause
        int num_lit_confl = 0;      // number of conflicting literals
        int non_waste_count = 0;
        
        // (1) inspect: check each literal of the clause
        for (int j = begin; j <= end; ++j) {
            const int p = literals[j];
            const int psign = LIT_SIGN(p);
            const char assignment = assignments[LIT_VAR(p) + offset];
            
            // decode the three cases: true, false, undefined
            const int a_true = (assignment==psign);
            const int a_undef = (assignment==LBOOL_UNDEF);
            const int a_false = !a_true && !a_undef;
            // anything defined is not wasted
            non_waste_count += !a_undef;          
            
            if (assignment == LBOOL_UNDEF) { // literal value is undefined
                num_lit_undef++;
            }
            else if (assignment == psign) { // literal value is true
                sat_flag = 1;
                waste_flag = 0;
                break; // clause is satisfied
            }
            else { // literal value is false
                num_lit_confl++;
                waste_flag = 0;
            }
        }
        
        // inspection results
        const int waste_flag2 = (non_waste_count == 0);
        if (waste_flag != waste_flag2) return;
        const int unit_flag = !sat_flag && !waste_flag && (num_lit_undef == 1);
        const int confl_flag = !sat_flag && (num_lit_undef == 0);
        const int unres_flag = !sat_flag && !waste_flag && (num_lit_undef > 1);
                
        // check flags are mutually exclusive
        const int sum = sat_flag + waste_flag + unit_flag + confl_flag + unres_flag;
        if (sum > 1) return;
        
        const int cid_ndx = (i * MULTI_FACTOR) + (tid % MULTI_FACTOR);
        // (2) process result: update output parameters
        if (sat_flag) {
            if (!sat_flag) return;
            atomic_inc(&stats[i * STAT_BLOCK_SIZE + STAT_OFFSET_SAT]);
        }
        else if (waste_flag == 1) {
            if (!waste_flag) return;
            atomic_inc(&stats[i * STAT_BLOCK_SIZE + STAT_OFFSET_WASTE]);
        }
        else if (num_lit_undef == 1) {
            if (!unit_flag) return;
            atomic_inc(&stats[i * STAT_BLOCK_SIZE + STAT_OFFSET_UNIT]);
            atomic_cmpxchg(&cid[cid_ndx], 0, -tid - 1);
        }
        else if (num_lit_undef == 0) {
            if (!confl_flag) return;
            atomic_inc(&stats[i * STAT_BLOCK_SIZE + STAT_OFFSET_CONFL]);
            atomic_max(&cid[cid_ndx], tid + 1);
        }
        else {
            if (!unres_flag) return;
            atomic_inc(&stats[i * STAT_BLOCK_SIZE + STAT_OFFSET_UNRES]);
        }
    }
}
