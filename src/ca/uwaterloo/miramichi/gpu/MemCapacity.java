package ca.uwaterloo.miramichi.gpu;

import java.util.function.Supplier;

/**
 * Specifies the maximum capacities, on both CPU and GPU, of a Mem. A -1 for hostCapacity indicates
 * a dynamically sized array on host. Clients can get later binding on capacity numbers by making
 * anonymous inner classes of this.
 */
public class MemCapacity {

  private final Supplier<Integer> hostCapacity;
  private final Supplier<Integer> deviceCapacity;

  public MemCapacity(final Supplier<Integer> hostCapacity, final Supplier<Integer> deviceCapacity) {
    this.hostCapacity = hostCapacity;
    this.deviceCapacity = deviceCapacity;
  }

  public MemCapacity(final int hc, final Supplier<Integer> dc) {
    this(() -> hc, dc);
  }

  public MemCapacity(final Supplier<Integer> capacity) {
    this(capacity, capacity);
  }

  public int hostCapacity() {
    return hostCapacity.get();
  }

  public int deviceCapacity() {
    return deviceCapacity.get();
  }
}
