/**
 * @file    opencl_query.cl
 * @author  Steven.Stewart@uwaterloo.ca
 */

/* ************************************************************************ */
//#include "packed_array.h"


        // 16 bit values
        #define bitwidth           16
        #define bitwidthLog2        4
        #define valuesPerCell       2
        #define valuesPerCellLog2   1
        #define valueMask       65535
        #define indexMask           1

        /*
        // 32 bits
        #define bitwidth           32
        #define bitwidthLog2        5
        #define valuesPerCell       1
        #define valuesPerCellLog2   0
        #define valueMask           0xFFFFFFFF
        #define indexMask           0
        */

        int cell(const int index) {
          // these expressions are equivalent when bitwidth is a power of 2
          // return index / valuesPerCell;
          return index >> valuesPerCellLog2;
        }

        int subcell(const int index) {
          return (index & indexMask) << bitwidthLog2;
        }

        int get2(__global const uint* const a, const int cell, const int subcell) {
          return (a[cell] >> subcell) & valueMask;
        }

        int get(__global const uint* const a, const int index) {
          return get2(a, cell(index), subcell(index));
        }


/* ************************************************************************ */

// Macros for literals
#define LIT_SIGN(p) ((p) & 1)
#define LIT_VAR(p) ((p) >> 1)

// Lifted Boolean domain
#define LBOOL_TRUE 0
#define LBOOL_FALSE 1
#define LBOOL_UNDEF 2

// Statistics
#define STAT_BLOCK_SIZE 5
#define STAT_OFFSET_SAT 0
#define STAT_OFFSET_UNIT 1
#define STAT_OFFSET_CONFL 2
#define STAT_OFFSET_UNRES 3
#define STAT_OFFSET_WASTE 4

/**
 * This kernel performs a step of BCP, called a query, using an OpenCL device. This step is executed for
 * multiple explorers, whose state (partial assignments) are stored in the assignments array. The result
 * of the query is either a clause id (unit or conflicting), or tha a fixed point has been reached. Some
 * statistical counts are gathered, which may also be part of the query result, when copied to the host.
 *
 * @param[in]   num_variables   Length of the variable assignment array.
 * @param[in]   assignments     State of all explorers (their assignments).
 * @param[in]   num_explorers   Number of explorers involved in the query.
 * @param[in]   num_literals    Number of literals in the clause database.
 * @param[in]   literals        Device memory address of the literals.
 * @param[in]   num_clauses     Number of clauses in the clause database.
 * @param[in]   clause_index    Index for looking up offset of a clause in literals.
 * @param[out]  cid             For each explorer, an encoded clause id of a unit or conflicting clause.
 *                              Result type indicated as follows: <0 = unit, >0 = conflict, 0 = done.
 * @param[out]  stats           For each explorer, statistics for number of unit, confl, sat, and "waste" clauses
 */
__kernel void opencl_query(
                           const int num_variables,
                           __global const uchar* const assignments,
                           const int num_explorers,
                           const int num_literals,
                           __global const uint* const literals,
                           //__global const uint* const literalsUnpacked,
                           const int num_clauses,
                           __global const int* const clause_index,
                           __global int* const cid,
                           __global int* const stats
                           )
{
  // Obtain thread id
  // tid encodes the explorerID and clauseID that we are going to work on
  // low bits are explorerID
  // high bits are clauseID
  const int tid = get_global_id(0);
  const int explorerID = tid / num_clauses;
  const int clauseID = tid - (explorerID * num_clauses);

  /*
  if (tid == 0) {
    printf("num_literals %d, num_clauses %d \n", num_literals, num_clauses);
  }
  */

  //const int offset = num_variables * explorerID;
  const int offset = NUM_VARIABLES_ALIGNED * explorerID;

  // set "begin" to be the offset of the first literal of this thread's clause
  const size_t begin = clause_index[clauseID];

  // set "end" to be the offset of the last literal of this thread's clause
  const size_t end = (clauseID == num_clauses - 1) ? (num_literals - 1) : (clause_index[clauseID + 1] - 1);

  int sat_flag = 0;           // flag for skipping "process result" step
  int waste_flag = (end - begin) > 1;     // flag indicating a wasted inspection
  int num_lit_undef = 0;      // number of unassigned literals in the clause

  // (1) inspect: check each literal of the clause
  for (int j = begin; j <= end; ++j) {
    const uint p = get(literals, j);
    /*
    const uint p2 = literalsUnpacked[j];
    if (p != p2) {
      printf("packing error at index %d: expected %d, actual %d \n", j, p2, p);
    }
    */
    const int psign = LIT_SIGN(p);
    const int var_offset = offset + LIT_VAR(p);
    const int remainder = (var_offset & 0x3) << 1;
    const uchar assignment_loc = assignments[var_offset >> 2];
    const uchar assignment = (assignment_loc >> remainder) & 0x3;
    if (assignment == LBOOL_UNDEF) { // literal value is undefined
      ++num_lit_undef;
    } else {
      waste_flag = 0;
    }
    if (assignment == psign) { // literal value is true
      sat_flag = 1;
      break; // clause is satisfied
    }
  }

  int confl_flag = num_lit_undef == 0 && !sat_flag;
  int unit_flag = num_lit_undef == 1 && !sat_flag;
  const int unres_flag = (sat_flag || confl_flag || unit_flag || waste_flag) ? 0 : 1;
  const int cid_ndx = (explorerID * MULTI_FACTOR) + (clauseID % MULTI_FACTOR);

  // (2) process result: update output parameters
#ifdef ASSERTIONS
  // record stats when assertions are enabled
  if (sat_flag) {
    atomic_inc(&stats[explorerID * STAT_BLOCK_SIZE + STAT_OFFSET_SAT]);
  }
  if (waste_flag) {
    atomic_inc(&stats[explorerID * STAT_BLOCK_SIZE + STAT_OFFSET_WASTE]);
  }
  if (unit_flag) {
    atomic_inc(&stats[explorerID * STAT_BLOCK_SIZE + STAT_OFFSET_UNIT]);
    atomic_cmpxchg(&cid[cid_ndx], 0, -clauseID - 1);
  }
  if (confl_flag) {
    atomic_inc(&stats[explorerID * STAT_BLOCK_SIZE + STAT_OFFSET_CONFL]);
    cid[cid_ndx] = clauseID + 1;
  }
  if (unres_flag) {
    atomic_inc(&stats[explorerID * STAT_BLOCK_SIZE + STAT_OFFSET_UNRES]);
  }
#else
  // assertions disabled, do not record stats
  if (unit_flag) {
    // unit
    atomic_cmpxchg(&cid[cid_ndx], 0, -clauseID - 1);
  }
  if (confl_flag) {
    // conflict
    cid[cid_ndx] = clauseID + 1;
  }
#endif
}
