package ca.uwaterloo.miramichi.seeding;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.List;

import ca.uwaterloo.miramichi.Explorer;

public class AssumptionSplitterSeedingStrategy extends SeedingStrategy {

  private final List<Integer> variableOrdering;
  private long state = 0;
  private final static int SIZE = 63;

  private final static List<Integer> defaultVariableOrdering;
  static {
    final List<Integer> a = new ArrayList<Integer>(SIZE);
    for (int i = 0; i < SIZE; i++) {
      a.add(i + 1);
    }
    defaultVariableOrdering = Collections.unmodifiableList(a);
  }

  public AssumptionSplitterSeedingStrategy() {
    this(defaultVariableOrdering);
  }

  public AssumptionSplitterSeedingStrategy(final List<Integer> vo) {
    this.variableOrdering = Collections.unmodifiableList(new ArrayList<Integer>(vo));
  }

  public ArrayList<Integer> nextAssumptions() {
    final BitSet b = BitSet.valueOf(new long[] {state});
    // System.out.println(state + " " + b + " " + b.length());
    final ArrayList<Integer> result = new ArrayList<Integer>(variableOrdering.size());
    for (int i = 0; i < b.length(); i++) {
      // for (int i = 0; i < variableOrdering.size(); i++) {
      // if (b.nextSetBit(i) == -1)
      // break;
      final int var = variableOrdering.get(i);
      final boolean polarity = b.get(i);
      // final int lit = polarity ? var : neg(var);
      final int lit = polarity ? 1 : 0;
      result.add(lit);
    }
    System.out.println(state + " " + result);
    // increment the state
    state++;
    // return result
    return result;
  }

  private static int neg(final int var) {
    // return Literal.neg(var);
    return -1 * var;
  }

  public boolean hasNextAssumptions() {
    return state <= variableOrdering.size();
  }

  @Override
  public boolean seed(final Explorer explorer) {
    if (!hasNextAssumptions()) {
      return false;
    } else {
      explorer.setAssumptions(nextAssumptions());
      return true;
    }
  }

  @Override
  public void reseed(Explorer ex, List<Integer> lits) {
    return;
  }

}
