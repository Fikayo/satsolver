package ca.uwaterloo.miramichi.seeding;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import ca.uwaterloo.miramichi.Explorer;
import ca.uwaterloo.miramichi.Literal;
import ca.uwaterloo.miramichi.MiniCDCL;
import ca.uwaterloo.miramichi.Var;

public class RandomVariableOrderingSeedingStrategy extends SeedingStrategy {

  private final int numberOfVarsToSeed;

  public RandomVariableOrderingSeedingStrategy() {
    this(4);
  }

  public RandomVariableOrderingSeedingStrategy(final int n) {
    numberOfVarsToSeed = n;
  }

  @Override
  public boolean seed(final Explorer explorer) {
    final Random r = new Random();
    final int varCount = explorer.database.varCount();
    final List<Integer> lits = new ArrayList<>(numberOfVarsToSeed);
    for (int i = 0; i < numberOfVarsToSeed; i++) {
      lits.add(Literal.var2lit(r.nextInt(varCount)));
    }
    reseed(explorer, lits);
    return true;
  }

  @Override
  public void reseed(final Explorer ex, final List<Integer> lits) {
    if (ex instanceof MiniCDCL) {
      reseed((MiniCDCL) ex, lits);
    } else {
      // TODO: how to reseed other kinds of Explorers?
      // TODO: implement Visitor pattern for double-dispatch?
      System.err.println("Not reseeding: " + ex);
    }
  }

  public void reseed(final MiniCDCL ex, final List<Integer> lits) {
    // TODO: communicate variable polarity as well as identity
    for (int i = 0; i < lits.size(); i++) {
      final int lit = lits.get(i);
      final int varID = Literal.var(lit);
      final Var v = ex.getVars()[varID];
      v.increaseActivityLevel(i + 2);
    }
  }
}
