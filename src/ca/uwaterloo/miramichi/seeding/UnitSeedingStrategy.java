package ca.uwaterloo.miramichi.seeding;

import java.util.ArrayList;
import java.util.List;

import ca.uwaterloo.miramichi.Clause;
import ca.uwaterloo.miramichi.Explorer;

/**
 * Finds all unit clauses.
 */
public class UnitSeedingStrategy extends SeedingStrategy {

  public UnitSeedingStrategy() {}

  public static ArrayList<Integer> unitLits(final Explorer explorer) {
    final ArrayList<Integer> units = new ArrayList<>();
    for (final Clause c : explorer.database.clauses) {
      if (c.size() == 1) {
        units.add(c.lit(0));
      }
    }
    return units;
  }

  @Override
  public boolean seed(final Explorer explorer) {
    final boolean result = explorer.setAssumptions(unitLits(explorer));
    assert result;
    return result;
  }

  @Override
  public void reseed(final Explorer ex, final List<Integer> lits) {
    return;
  }
}
