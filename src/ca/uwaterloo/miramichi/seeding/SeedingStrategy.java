package ca.uwaterloo.miramichi.seeding;

import java.util.List;

import ca.uwaterloo.miramichi.Explorer;


/**
 * Interface for seeding strategies. A SeedingStrategy initializes (mutates) an explorer that has
 * not started exploring yet. This might involve setting assumptions, setting an initial VSIDS
 * variable ordering, etc.
 *
 */
public abstract class SeedingStrategy {

  public SeedingStrategy() {}

  /**
   * Seed/initialize the given explorer.
   *
   * @param explorer
   * @return true if the explorer was seeded
   */
  public abstract boolean seed(final Explorer explorer);

  public abstract void reseed(Explorer ex, List<Integer> lits);

}
