package ca.uwaterloo.miramichi;

import java.util.List;

/**
 * A query result captures information returned by a {@link QueryEngine query engine} in response to
 * a query.
 */
public final class QResult {

  public static enum Type {
    CONFLICT, UNIT, SAT, UNRESOLVED
  };

  /*
   * Below are the metadata and content of a query result. The metadata includes the result type,
   * and the content consists of clauses.
   */
  public final Type type;
  public final int[] clause_ids;
  public final int[] unit_literals;

  /**
   * Constructs a a query result.
   *
   * @param type the type of the result (unit, confl, or done)
   * @param clause_ids clauses ids singled out by the query result
   * @param unit_literals if the query type is unit, this contains the unit information
   * @param total_sat the total number of satisfied clauses
   * @param total_unres the total number of unresolved clauses
   * @param total_unit the total number of unit clauses
   * @param total_confl the total number of conflicting clauses
   * @param total_waste the total number of wasted clauses (they didn't need to be inspected)
   */
  public QResult(final Type type, final int[] clause_ids, final int[] unit_literals) {

    this.type = type;

    // set clause IDs and unit literals
    if (type.equals(Type.SAT) || type.equals(Type.UNRESOLVED)) {
      this.clause_ids = null;
      this.unit_literals = null;
      return;
    } else {
      // CONFLIT or UNIT
      assert type.equals(Type.CONFLICT) || type.equals(Type.UNIT);
      this.clause_ids = clause_ids;
      assert this.clause_ids != null && this.clause_ids.length > 0;
    }

    // If the type is unit, and the unit literals were not recorded by the query,
    // then inspect each clause in clause_ids to determine the unit literal, adding
    // the unit literals to this.unit_literals
    if (type.equals(Type.UNIT) && unit_literals == null) {
      this.unit_literals = new int[clause_ids.length];
      for (int i = 0; i < clause_ids.length; i++) {
        // TODO: determine which literal of the clause(s) is/are unit
      }
      assert false : "unit literals were not specified in construction of QResult";
    } else {
      this.unit_literals = unit_literals;
    }
  }

  public QResult(final Type type, final List<Integer> clause_ids, final int[] unit_literals) {
    this(type, Util.list2array(clause_ids), unit_literals);
  }

  public QResult(final Type type, final List<Integer> clause_ids, final List<Integer> unit_literals) {
    this(type, Util.list2array(clause_ids), Util.list2array(unit_literals));
  }

  public boolean isConflict() {
    return Type.CONFLICT.equals(type);
  }

  public boolean isUnit() {
    return Type.UNIT.equals(type);
  }

  public boolean isSAT() {
    return Type.SAT.equals(type);
  }

  public boolean isUnresolved() {
    return Type.UNRESOLVED.equals(type);
  }
}
